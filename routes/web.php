<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/////---------Frontend -----------


Route::get('','frontend\IndexController@getIndex' );

Route::get('gioithieu','frontend\IndexController@getGioithieu' );
//Route::get('sanpham','frontend\IndexController@getSanpham' );
Route::get('san-pham{id}','frontend\SpController@getSp' );
Route::get('tintuc','frontend\IndexController@getTintuc' );
Route::get('tintuc/{id}','frontend\IndexController@getShowtintuc' );
Route::get('lienhe','frontend\IndexController@getLienhe' );
Route::get('hdsd','frontend\IndexController@getHdsd' );
Route::get('hdsd/{id}','frontend\IndexController@getHdsd_cate' );
Route::get('thanhtoan','frontend\IndexController@getThanhtoan' );
// Route::get('sanpham/{id}','frontend\SpController@getSp' );





Route::get('login','LoginController@getLogin')->middleware('CheckLogout');
Route::post('login','LoginController@postLogin');
Route::get('logout', 'LoginController@getLogout');
Route::group(['prefix'=>'admin','middleware'=>'CheckLogin'],function(){
   Route::get('','IndexController@getIndex');
   Route::group(['prefix'=>'category'],function(){
   	    //admin/category/list
      Route::get('add','CategoryController@getAdd');
      Route::post('add','CategoryController@postAdd')->name('post.cate');
      Route::get('list','CategoryController@getList');
      Route::get('edit/{idCate}','CategoryController@getEdit');
      Route::post('edit/{idCate}','CategoryController@postEdit');
      Route::get('del/{idCate}','CategoryController@postDel');

   });
     Route::group(['prefix'=>'content'],function(){  
        Route::get('add','ContentController@getAdd');
        Route::post('add','ContentController@postAdd');
        Route::get('list','ContentController@getList');
        Route::get('edit/{id}','ContentController@getEdit');
        Route::post('edit/{id}','ContentController@postEdit');
        Route::get('del/{id}','ContentController@getDel');

   });
      Route::group(['prefix'=>'customer'],function(){  	    
          Route::get('add','CustomerController@getAdd');
          Route::post('add','CustomerController@postAdd');
          Route::get('list','CustomerController@getList');

          Route::get('edit/{id}','CustomerController@getEdit');
           Route::post('edit/{id}','CustomerController@postEdit');

          Route::get('delete/{id}','CustomerController@getDelete');
   });
   /// link download
      Route::group(['prefix'=>'download'],function(){
          Route::get('add','DownloadController@getAdd');
          Route::post('add','DownloadController@postAdd');
          Route::get('list','DownloadController@getList');
          Route::get('edit/{id}','DownloadController@getEdit');
          Route::post('edit/{id}','DownloadController@postEdit');
          Route::get('del/{id}','DownloadController@Del');


   });
            Route::group(['prefix'=>'information'],function(){
          Route::get('index','InformationController@index');
          Route::get('create','InformationController@create');
           Route::post('store','InformationController@store');
          Route::get('edit/{id}','InformationController@getEdit');
           Route::post('edit/{id}','InformationController@postEdit');
         
   });
   /// tin tức
      Route::group(['prefix'=>'post'],function(){ 	 
          Route::get('add','PostController@getAdd');
          Route::post('add','PostController@postAdd');
          Route::get('list','PostController@getList');
          Route::get('edit/{id}','PostController@getEdit');
          Route::post('edit/{id}','PostController@postEdit');
          Route::get('delete/{id}','PostController@getDelete');
   });
      // phần Product
      Route::group(['prefix'=>'product'],function(){  	
          Route::get('list','ProductController@getList');
          Route::get('add','ProductController@getAdd');
          Route::post('add','ProductController@postAdd');
          Route::get('edit/{id}','ProductController@getEdit');
          Route::post('edit/{id}','ProductController@postEdit');
          Route::get('delete/{id}','ProductController@getDelete');

   });
      // phần Product_Package
      Route::group(['prefix'=>'product_package'],function(){    
          Route::get('list','Product_packageController@getList');

          Route::get('add','Product_packageController@getAdd');
          Route::post('add','Product_packageController@postAdd');

          Route::get('edit/{id}','Product_packageController@getEdit');
          Route::post('edit/{id}','Product_packageController@postEdit');

          Route::get('delete/{id}','Product_packageController@getDelete');

   });
      //Phần Slide
      Route::group(['prefix'=>'slide'],function(){  
          Route::get('list','SlideController@getList');	   

          Route::get('add','SlideController@getAdd');
          Route::post('add','SlideController@postAdd');

          Route::get('edit/{id}','SlideController@getEdit');
          Route::post('edit/{id}','SlideController@postEdit');

          Route::get('delete/{id}','SlideController@getDelete');


   });
        //Phần Doitac_image
        Route::group(['prefix'=>'doitac'],function(){  
         Route::get('list','DoitacController@getList');	   

         Route::get('add','DoitacController@getAdd');
         Route::post('add','DoitacController@postAdd');

         Route::get('edit/{id}','DoitacController@getEdit');
         Route::post('edit/{id}','DoitacController@postEdit');

         Route::get('delete/{id}','DoitacController@getDelete');


  });
      Route::group(['prefix'=>'member'],function(){   	    
          Route::get('add','MemberController@getAdd');
          Route::get('list','MemberController@getList');
          Route::get('edit','MemberController@getEdit');

   });
   ///--dac diem noi bat -----
   Route::group(['prefix'=>'highlight'],function(){         
      Route::get('add','HighlightController@getAdd');
      Route::post('add','HighlightController@postAdd');
      
      Route::get('list','HighlightController@getList');

      Route::get('edit/{id}','HighlightController@getEdit');
      Route::post('edit/{id}','HighlightController@postEdit');

      Route::get('delete/{id}','HighlightController@getDel');

});

   
});