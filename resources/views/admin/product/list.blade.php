 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                     @if(session('thongbao'))
                        <div class="success alert-success">
                            {{session('thongbao')}}
                        </div>
                     @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Description</th>
                                <th>Featue</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $pr)
                            <tr class="odd gradeX" align="center">
                                <td>{{$pr->id}}</td>
                                <td>{{$pr->name}}</td>
                                <td>
                                    <p><img width="200px" src="uploads/product/{{$pr->image}}" alt=""></p>
                                </td>
                                <td>{{$pr->description}}</td>
                                <td>{{$pr->featue}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                    <a onclick="return confirm('Bạn có chắc chắn muốn xóa không?')" href="admin/product/delete/{{$pr->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> 
                                    <a onclick="return confirm('Bạn có chắc chắn muốn sửa không?')" href="admin/product/edit/{{$pr->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection