 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>{{$product->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif

                    @if(session('thongbao'))
                         <div class="alert alert-success">
                             {{session('thongbao')}}
                         </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/product/edit/{{$product->id}}" method="POST" enctype="multipart/form-data">
                          <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <label> Name</label>
                                <input class="form-control" name="name" placeholder="Nhập tên" value="{{$product->name}}" />
                            </div>
                            <div class="form-group">
                                <label> Giới Thiệu</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="content" placeholder="Nhập nội dung">
                                    {{$product->content}}
                                </textarea>   
                                {!! ShowError($errors,'content')!!}                        
                            </div>
                            <div class="form-group">
                                <label> Báo Giá</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="content1" placeholder="Nhập nội dung">
                                    {{$product->content1}}    
                                </textarea>   
                                {!! ShowError($errors,'content')!!}                        
                            </div>
                            
                            <button type="submit" class="btn btn-default">Sửa</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection