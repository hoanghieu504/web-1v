 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thêm sản phẩm
                           
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    {{--  @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            @foreach($errors->all() as $err)
                                {{$err}}<br>
                            @endforeach
                        </div>
                    @endif  --}}

                    @if(session('thongbao'))
                         <div class="alert alert-success">
                             {{session('thongbao')}}
                         </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/product/add" method="POST" > @csrf
                          
                            <div class="form-group">
                                <label> Name</label>
                                <input class="form-control" name="name" placeholder="Nhập tên" />
                                {!! ShowError($errors,'name')!!}
                            </div>
                            <div class="form-group">
                                <label> Giới Thiệu</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="content" placeholder="Nhập nội dung"></textarea>   
                                {!! ShowError($errors,'content')!!}                        
                            </div>
                            <div class="form-group">
                                <label> Báo Giá</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="content1" placeholder="Nhập nội dung"></textarea>   
                                {!! ShowError($errors,'content')!!}                        
                            </div>
                            {{--  <div class="form-group">
                                <label>Image</label>
                                <input width="200px" class="form-control" type="file" name="image" />
                            </div>
                            <div class="form-group">
                                <label> Description </label>
                                <textarea style="resize: none" rows="8" class="form-control" name="description" placeholder="Mô tả danh mục"></textarea>                           
                            </div>
                            <div class="form-group">
                                <label> Featue </label>
                                <textarea style="resize: none" rows="8" class="form-control" name="featue"
                                 placeholder="Đặc tính danh mục"></textarea>                           
                            </div>  --}}
                            
                            <button type="submit" class="btn btn-default">Thêm</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection