 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Thêm link tải
                           
                        </h1>
                        
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                    <div class="alert alert-success">
                       <b>{{session('thongbao')}}</b>
                    </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST"> @csrf
                          
                            <div class="form-group">
                                <label>Tên Phần Mềm</label>
                                <input class="form-control" name="name" placeholder="Tên phần mềm" />
                            </div>
                            <div class="form-group">
                                <label>Chèn link tải</label>
                                <input class="form-control" name="link" placeholder="Chèn link tải" />
                            </div>
                            <div class="form-group">
                                <label>Mô tả</label>
                                <textarea  name="content" class="form-control" rows="3"> </textarea>
                            </div>
                            
                            <button type="submit" class="btn btn-default">Category Add</button>
                            
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection