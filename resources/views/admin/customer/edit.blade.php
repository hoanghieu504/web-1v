 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Customer
                            <small>Edit</small>
                        </h1>
 @if(count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                        {{$err}}<br>
                        @endforeach
                    </div>
                    @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/customer/edit/{{$Customer->id}}" method="POST">
                            @csrf
                            
                            <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" name="title" value="{{$Customer->title}}" placeholder="Please Enter Category Name" />
                            </div>
                          
                            <div class="form-group">
                                <label>Content</label>
                                <textarea name="content" class="form-control" rows="5">{{$Customer->content}}</textarea>
                            </div>
                        
                            <button type="submit" class="btn btn-default">EDit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection