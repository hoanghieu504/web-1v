 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>List</small>
                        </h1>
                        @if(session('thongbao'))
                        <div class="alert alert-succcess">
                           <b>{{session('thongbao')}}</b>
                        </div>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Title</th>
                                <th>Content</th>
                             
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Customers as $value)
                            <tr class="odd gradeX" align="center">

                                <td>{{$value->id}}</td>
                                <td>{{$value->title}}</td>
                                <td>{{$value->content}}</td>
                              
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/customer/delete/{{$value->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/customer/edit/{{$value->id}}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @if($Customers->count()<3)

                    <button type="button" class="btn btn-danger"><a href="admin/customer/add">Thêm mới</a></button>
                    @endif
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection