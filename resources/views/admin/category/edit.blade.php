@extends('admin.layout.index')
 @section('content')
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>Edit</small>
                        </h1>
                    </div>
                     
                    @if (session('thongbao'))
                    <div class="alert bg-danger" role="alert">
                        <strong>  {{session('thongbao')}}</strong>
                    </div>
                    @endif
                     <div class="col-lg-7" style="padding-bottom:120px">
                       
                        <form action="{{ route('post.cate') }}" method="POST"> @csrf
                            <div class="form-group">
                                <label>Category Parent</label>
                                <select class="form-control" name="parent">
                                    <option value="0">Thêm danh mục hướng dẫn</option>
                                    {{GetCategory($categories,0,'',$cate->parent)}}
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Category Name</label>
                                <input value="{{$cate->category_name}}" class="form-control" name="txtCateName" placeholder="Please Enter Category Name" />
                                {!! ShowError($errors,'txtCateName')!!}
                            </div>
                            
                            
                           
                           
                            <button type="submit" class="btn btn-default">Lưu lại</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection