 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product_package
                            <small>Danh sách</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                     @if(session('thongbao'))
                        <div class="success alert-success">
                            {{session('thongbao')}}
                        </div>
                     @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                
                                <th>Tên gói sản phẩm</th>
                                <th>Image</th>
                                <th>Giá</th>
                                <th>Mô tả</th>
                                <th>Giá Sản Phẩm</th>
                                <th>Sản Phẩm</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($product_package as $value)
                            <tr class="odd gradeX" align="center">
                                <td>{{$value->id}}</td>
                                 <td>{{$value->name}}</td>
                                <td>
                                  <p><img width="100px" src="image/{{$value->image}}" alt=""></p>
                                </td>
                              
                                <td>{{number_format($value->price,0,"",".")}} VND</td>
                                <td>{!!$value->description!!}</td>
                                 <td>{{$value->price}}</td>
                                 <td>{{$value->Product->name}}</td>
                                

                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                   <a onclick="return confirm('Bạn có chắc chắn muôn xóa không?')" href="admin/product_package/delete/{{$value->id}}"> Delete</a></td>

                                <td class="center"><i class="fa fa-pencil fa-fw"></i>
                                   <a  href="admin/product_package/edit/{{$value->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection