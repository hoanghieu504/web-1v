 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thêm gói sản phẩm
                           
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                    

                        @if(session('thongbao'))
                            <div class="success alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="admin/product_package/add" method="POST" enctype="multipart/form-data"> @csrf
                                                    
                            <div class="form-group">
                                <label> Sản Phẩm</label>
                                <select name="sanpham" class="form-control input-sm m-bot15">
                                    @foreach($product as $pro)
                                    <option value="{{$pro->id}}">{{$pro->name}}</option>
                                    @endforeach
                               </select>
                           </div>
                            <div class="form-group">
                                <label>Option Sản Phẩm</label>
                                <input class="form-control" name="name" placeholder="Nhập tên sản phẩm" />
                                {!! ShowError($errors,'name')!!}
                            </div>
                            <div class="form-group">
                                <label>Giá Sản Phẩm</label>
                                <input class="form-control" name="price" placeholder="Nhập giá sản phẩm" />
                                {!! ShowError($errors,'price')!!}
                            </div>

                            <div class="form-group">
                                <label>Image</label>
                                <input class="form-control" type="file" name="image" />
                                {!! ShowError($errors,'file')!!}   
                            </div>
                            <div class="form-group">
                                <label> Mô tả sản phẩm</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="description" placeholder="Nhập mô tả sản phẩm"></textarea>   
                                {!! ShowError($errors,'description')!!}                        
                            </div>
                             <div class="form-group">
                                <label> Chi tiết sản phẩm</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="content" placeholder="Nhập nội dung"></textarea>   
                                {!! ShowError($errors,'content')!!}                        
                            </div>

                            {{--  <div class="form-group">
                                <label> State </label>
                                <select name="state" class="form-control input-sm m-bot15">
                                    <option name="state" value="0" type="radio">Ẩn</option>
                                    <option name="state" value="1" type="radio">Hiển thị</option>
                                </select>
                            </div>  --}}

                           

                            <button type="submit" class="btn btn-default">Thêm</button>
                            
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection