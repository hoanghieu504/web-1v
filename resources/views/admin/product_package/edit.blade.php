 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product_package
                            <small>{{$product_package->name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                       

                        @if(session('thongbao'))
                            <div class="success alert-success">
                                {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="admin/product_package/edit/{{$product_package->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">                            
                            <div class="form-group">
                                <label> Sản Phẩm</label>
                                <select name="sanpham" class="form-control input-sm m-bot15">
                                    @foreach($Product as $pro)
                                    <option value="{{$pro->id}}">{{$pro->name}}</option>
                                    @endforeach
                               </select>
                           </div>
                            <div class="form-group">
                                <label>Option Sản Phẩm</label>
                                <input class="form-control" name="name" value="{{$product_package->name}}" placeholder="Nhập tên sản phẩm" />
                                {!! ShowError($errors,'name')!!}
                            </div>
                            <div class="form-group">
                                <label>Giá Sản Phẩm</label>
                                <input class="form-control" name="price" value="{{$product_package->price}}" placeholder="Nhập giá sản phẩm" />
                                {!! ShowError($errors,'price')!!}
                            </div>

                            <div class="form-group">
                                <label>Image</label>
                                <p> <img src="image/{{$product_package->image}}" > </p>
                                <input  class="form-control" type="file" name="image" />
                                
                            </div>
                            <div class="form-group">
                                <label> Mô tả sản phẩm</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="description" placeholder="Nhập mô tả sản phẩm">
                                    {{$product_package->description}}
                                </textarea>   
                                {!! ShowError($errors,'description')!!}                        
                            </div>
                             <div class="form-group">
                                <label> Chi tiết sản phẩm</label>
                                <textarea style="resize: none" rows="8" class="form-control" name="content" placeholder="Nhập nội dung">
                                {!!$product_package->content!!}
                                </textarea>   
                                {!! ShowError($errors,'content')!!}                        
                            </div>

                            <button type="submit" class="btn btn-default">Sửa</button>
                            
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection