 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sửa bài viết
                           
                        </h1>
                    @if(count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                        {{$err}}<br>
                        @endforeach

                    </div>
                    @endif
                     @if(session('thongbao'))
                        <div class="alert alert-succcess">
                           <b>{{session('thongbao')}}</b>
                        </div>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/post/edit/{{$post->id}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            
                            <div class="form-group">
                                <label>Tiêu Đề Bài Viết</label>
                                <input class="form-control" name="title" placeholder="Please Enter Category Name" value="{{$post->title}}" />
                                {!! ShowError($errors,'title')!!}
                            </div>
                            <div class="form-group">
                                <label>Ảnh Tiều Đề</label>
                                <p> <img src="image/{{$post->image}}" > </p>
                                <input width="100px" type="file" class="form-control" name="image">
                                {!! ShowError($errors,'file')!!}
                            </div>
                            <div class="form-group">
                                <label>Nội Dung Bài Viết</label>
                                <textarea name="content" class="form-control" rows="10">{{$post->contents}}</textarea>
                                {!! ShowError($errors,'title')!!}
                            </div>
                           <div class="form-group">
                                <label>Tóm Tắt Bài Viết</label>
                                <textarea name="content1" class="form-control" rows="10">{{$post->content}}</textarea>
                                {!! ShowError($errors,'content1')!!}
                            </div>
                        
                            <button type="submit" class="btn btn-default">Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection