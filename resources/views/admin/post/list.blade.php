 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh sách bài viết
                            
                        </h1>
                      
                        @if (session('thongbao'))
                        <div class="alert bg-success" role="alert">
                            <strong>  {{session('thongbao')}}</strong>
                        </div>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        
                        <thead>
                            
                            <tr align="center">
                               
                                <th>ID</th>
                                
                                <th>Tiêu Đề Bài Viết</th>
                                <th>Ảnh/th>
                                <th>Nội Dung</th>
                             
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($posts as $value)
                            <tr class="odd gradeX" align="center">

                                <td>{{$value->id}}</td>
                               
                                <td>{{$value->title}}</td>
                                <td>  <img width="100px" src="image/{{$value->image}}" /></td>
                                <td><p  style="width: 500px; height:40px; overflow: hidden;">{!!$value->content!!}</p></td>
                              
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a onclick="return confirm('Bạn có chắc chắn muốn xóa không?')" href="admin/post/delete/{{$value->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/post/edit/{{$value->id}}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <button type="button" class="btn btn-danger"><a href="admin/post/add">Thêm mới</a></button>

                   
                  
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection