<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Category<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/category/list">List category</a>
                                </li>
                                <li>
                                    <a href="admin/category/add">Add category</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/product/list"><i class="fa fa-cube fa-fw"></i> Product<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/product/list">List Product</a>
                                </li>
                                <li>
                                    <a href="admin/product/add">Add Product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> Content<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/content/list">List content</a>
                                </li>
                                <li>
                                    <a href="admin/content/add">Add content</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/customer/list"><i class="fa fa-users fa-fw"></i> customer<span class="fa arrow"></span></a>
                          
                            <!-- /.nav-second-level -->
                        </li>
                     
                         <li>
                            <a href="admin/post/list"><i class="fa fa-users fa-fw"></i>post<span class="fa arrow"></span></a>
                          
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> member <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/member/list">List member</a>
                                </li>
                                <li>
                                    <a href="admin/member/add">Add member</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="admin/highlight/list"><i class="fa fa-users fa-fw"></i> Điểm nổi bật - Tại sao<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/highlight/list">Danh sách </a>
                                </li>
                                <li>
                                    <a href="admin/highlight/add">Thêm </a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                         <li>
                            <a href="admin/product_package/list"><i class="fa fa-users fa-fw"></i> Product_package <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/product_package/list">List product</a>
                                </li>
                                <li>
                                    <a href="admin/product_package/add">Add product</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                          <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> slide <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/slide/list">List slide</a>
                                </li>
                                <li>
                                    <a href="admin/slide/add">Add slide</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         
                            <li>
                                <a href="#"><i class="fa fa-users fa-fw"></i> Logo đối tác<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="admin/doitac/list">Danh sách logo</a>
                                    </li>
                                    <li>
                                        <a href="admin/doitac/add">Thêm logo</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-users fa-fw"></i>Dowload<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="admin/download/list">Danh sách Dowload</a>
                                    </li>
                                    <li>
                                        <a href="admin/download/add">Thêm Phần mềm</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                            <a href="#"><i class="fa fa-users fa-fw"></i> user <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="admin/user/list">List user</a>
                                </li>
                                <li>
                                    <a href="admin/user/add">Add user</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                         <li>
                            <a href="admin/information/index"><i class="fa fa-users fa-fw"></i>Giới thiệu công ty<span class="fa arrow"></span></a></li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
