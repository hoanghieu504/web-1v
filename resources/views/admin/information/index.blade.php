 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">POSTS
                            <small>List</small>
                        </h1>
                        @if(session('thongbao'))
                        <div class="alert alert-succcess">
                           <b>{{session('thongbao')}}</b>
                        </div>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                               
                                <th>logo</th>
                                <th>tên công ty</th>
                                <th>Địa chỉ đăng kí</th>
                              <th>Địa chỉ hoạt động</th>
                                <th>Mã số thuế</th>
                                 <th>Điện thoại</th>
                                <th>Hotline</th>
                                <th>email</th>
                                 <th>Website</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($information as $value)
                            <tr class="odd gradeX" align="center">

                               
                                <td><img src="{{asset('public/upload/logo/'.$value->logo)}}" style="width:50px;height:50px"></td>
                                <td>{{$value->name}}</td>

                              <td>{{$value->registerd_address}}</td>
                             <td>{{$value->active_address}}</td>
                              <td>{{$value->tax}}</td>
                             <td>{{$value->phone}}</td>
                              <td>{{$value->hotline}}</td>
                               <td>{{$value->email}}</td>
                               <td>{{$value->website}}</td>
                               
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/information/edit/{{$value->id}}">Edit</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                   

                  
                    @if($information->count()==0)
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/information/create">Thêm</a></td>
                                @endif
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection