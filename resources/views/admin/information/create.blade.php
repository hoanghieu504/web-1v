 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Thông tin công ty
                            <small>Add</small>
                        </h1>
 @if(count($errors)>0)
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                        {{$err}}<br>
                        @endforeach

                    </div>
                    @endif
                     @if(session('thongbao'))
                        <div class="alert alert-succcess">
                           <b>{{session('thongbao')}}</b>
                        </div>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="admin/information/store" method="POST" enctype="multipart/form-data">
                            @csrf
                            
                           
                           <div class="form-group">
                                <label>Logo</label>
                                  
                                <input class="form-control" name="logo" type="file"/>
                            </div>
                             <div class="form-group">
                                <label>Tên công ty</label>
                                <input class="form-control" name="name"  placeholder="Please Enter Category Name" />
                            </div>
                             <div class="form-group">
                                <label>Địa chỉ đăng kí</label>
                                <input class="form-control" name="registerd_address" placeholder="Please Enter Category Name" />
                            </div>
                               <div class="form-group">
                                <label>Địa chỉ hoạt động</label>
                                <input class="form-control" name="active_address" placeholder="Please Enter Category Name" />
                            </div>
                               <div class="form-group">
                                <label>Mã số thuế</label>
                                <input class="form-control" name="tax" placeholder="Please Enter Category Name" />
                            </div>
                               <div class="form-group">
                                <label>Điện thoại</label>
                                <input class="form-control" name="phone" placeholder="Please Enter Category Name" />
                            </div>
                               <div class="form-group">
                                <label>Hotline</label>
                                <input class="form-control" name="hotline" placeholder="Please Enter Category Name" />
                            </div>
                             <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" placeholder="Please Enter Category Name" />
                            </div>
                             <div class="form-group">
                                <label>Website</label>
                                <input class="form-control" name="website" placeholder="Please Enter Category Name" />
                            </div>



                            <div class="form-group">
                                <label>Bản đò</label>
                                <textarea name="map" class="form-control" rows="10"></textarea>
                            </div>
                          <div class="form-group">
                                <label>Giới thiệu</label>
                                 <textarea name="introduce" class="form-control" rows="10"></textarea>
                            </div>
                            <button type="submit" class="btn btn-default">Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection