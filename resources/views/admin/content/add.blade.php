 @extends('admin.layout.index')
 @section('content')
  <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Nội dung danh mục
                            
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                    <div class="alert bg-success" role="alert">
                        <strong>  {{session('thongbao')}}</strong>
                    </div>
                    @endif
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="" method="POST"> @csrf
                            <div class="form-group">
                                <label>Chọn danh mục</label>
                                <select class="form-control" name="category">
                                    <option value="0" >-- Chọn danh mục hướng dẫn sử dụng--</option>
                                    {{GetCategory($categories,0,'',0)}}
                                </select>
                                {!!ShowError($errors,'category')!!}
                            </div>
                            <div class="form-group">
                                <label>Miêu tả</label>
                                <textarea id="content"  name="description" style="width: 100%;height: 100px;">

                                    
                                </textarea>
                                {!!ShowError($errors,'description')!!}
                               
                            </div>  
                            <div class="form-group">
                                <label>Thêm Link youtube</label>
                                <input class="form-control" name="link" placeholder="Please Enter Category Order" />
                                {!!ShowError($errors,'link')!!}
                            </div>
                            
                           
                            <div class="form-group">
                               
                            </div>
                            <button type="submit" class="btn btn-default">Thêm</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
         
@endsection