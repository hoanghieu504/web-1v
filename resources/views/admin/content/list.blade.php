 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh sách nội dung
                            
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if (session('thongbao'))
                    <div class="alert bg-success" role="alert">
                        <strong>  {{session('thongbao')}}</strong>
                    </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Danh Mục</th>
                                {{--  <th>Nội dung</th>  --}}
                                <th>Link youtube</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($content as $value)
                                
                           
                            <tr class="odd gradeX" align="center">
                                <td>{{$value->id}}</td>
                                <td>{{$value->category->category_name}}</td>
                                {{--  <td>{{$value->content}}</td>  --}}
                                <td>{{$value->links}}</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i>
                                    <a onclick="return confirm('Bạn có chắc chắn muốn xóa không?')" href="admin/content/del/{{$value->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> 
                                    <a onclick="return confirm('Bạn có chắc chắn muốn sửa không?')" href="admin/content/edit/{{$value->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection