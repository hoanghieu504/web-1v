@extends('admin.layout.index')
 @section('content')
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">HighLight
                            <small>Edit</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                         @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                {{$err}}<br>
                                @endforeach
                            </div>
                        @endif

                        @if(session('thongbao'))
                            <div class="alert alert-success">
                                 {{session('thongbao')}}
                            </div>
                        @endif
                        <form action="admin/highlight/edit/{{$highlight->id}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                           
                            <div class="form-group">
                                <label>Tiêu đề</label>
                                <input class="form-control" name="title" placeholder="Please Enter Title" value=" {{$highlight->title}}" />
                            </div>
                            <div class="form-group">
                                <label>Nội dung</label>
                                  <textarea style="resize: none" rows="8" class="form-control" name="content" 
                                 />{!!$highlight->content!!}</textarea> 
                            </div>
                             <div class="form-group">
                                <label>Ảnh</label>
                                <p><img src="{{asset('frontend/image/' .$highlight->image)}}" width="100px" height="100px" alt=""></p>
                                <input class="form-control" type="file" name="image" />

                                @if(session('loi'))
                                    <div class="danger alert-danger">
                                         {{session('loi')}}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>Trạng thái</label>
                                 <select class="form-control" name="status">
                                    <option >Chọn trạng thái của bài</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select>
                            </div>

                            <button type="submit" class="btn btn-default">Highlight Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection