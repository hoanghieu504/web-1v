 @extends('admin.layout.index')
 @section('content')<!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">HighLight
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    @if(session('thongbao'))
                        <div class="alert alert-success">
                             {{session('thongbao')}}
                        </div>
                    @endif
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">

                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tiêu đề</th>
                                <th>Nội dung</th>
                                 <th>Ảnh</th>
                                <th>Trạng thái</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                              @foreach($highlight as $hl)
                            <tr class="odd gradeX" align="center">
                                <td>{{$hl->id}}</td>
                                
                                <td>{{$hl->title}}</td>
                                <td>{!!$hl->content!!}</td>
                                <td><img src="{{asset('frontend/image/' .$hl->image)}}" width="100px" height="100px" alt=""></td>
                                <td>
                                    {{$hl->status}}
                                </td>

                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/highlight/delete/{{$hl->id}}"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/highlight/edit/{{$hl->id}}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
@endsection