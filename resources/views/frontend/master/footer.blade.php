<div class="container">
    <div class="row">
        <div class="ft_ttlienhe col-sm">
            <div class="row ft_title">
                <div class="w-100 text-center col-12 col-sm-12 col-md-2 ft_img">
                    <img src="image/logosag.png" alt="">
                </div>
                <div style="padding:0px" class="col-12 col-sm-12 col-md-10">
                    <p>CÔNG TY TNHH GIẢI PHÁP PHẦN MỀM SAG</p>
                </div>
            </div>
            <div class="ft_lh">
                <p><span>ĐC đăng ký:</span> Số 48 Vạn Phúc Thượng, Tổ 04, cụm 05, P.Cống Vị, Q. Ba Đình, TP. Hà
                Nội</p>
                <p><span>ĐC hoạt động:</span> Nhà 27-28/TT23, KTD Văn Phú, P.Phú La, Q.Hà Đông, TP. Hà Nội</p>
                <p><span>Mã số thuế:</span> 0108 481 637</p>
                <p><span>Tổng đài:</span> 1900 2658</p>
                <p><span>Hotline:</span> 0986.807.407</p>
                <p><span>Email:</span> info@sagvietnam.com</p>
                <p><span>Website:</span> <a href="http://sagvietnam.com/" target="_blank">http://sagvietnam.com/</a></p>
</div>
            <div class="ft_mxh">
                <a href="##"><i class="fab fa-facebook-square"></i></a>
                <a href="##"><i style="color: aqua;" class="fas fa-phone-square-alt"></i></a>
                <a href="##"><i style="color: #cd3082" class="fab fa-invision"></i></a>
                <a href="##"><i style="color: red;" class="fab fa-youtube"></i></a>
            </div>
        </div>
        <div class="col-sm">
            <div class="row ft_title">
                <div class="col-12">
                    <p>CÁC GÓI DỊCH VỤ</p>
                </div>
            </div>
            <div class="ft_center_list">
                @foreach ($package as $item)
                    
                
                <p><a href="##">{{$item->name}}</a></p>
                @endforeach
            </div>
        </div>
        <div class="col-sm">
            <div class="row ft_title">
                <div class="col-12">
                    <p>TIN TỨC</p>
                </div>
            </div>
            <div class="ft_center_list">
                @foreach ($post as $item)
                <p><a href="tintuc/{{$item->id}}"> {{$item->title}}</a></p>
                   
                @endforeach
               
                        
            </div>
        </div>
    </div>
</div>
<div class="ft_cpr">
    <span>Copyright © 2019 All rights reserved.</span>
</div>
