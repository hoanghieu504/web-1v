<nav>
    <!-- Button trigger modal -->
    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
        Launch demo modal
    </button> -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</nav>
<nav class="menuheader">
    <div class="container">
        <div class="row">
            <div class="mnhd-img col-12 col-sm-12 col-md-2">
                <a href="/"><img src="image/logosag.png" alt=""></a>
            </div>
            <div class="col-12 col-sm-12 col-md-10">
                <div>
                    <ul class="nav justify-content-end list_menuheader">
                        <li class="nav-item">
                            <a class="nav-link active" href="#"><i class="fas fa-shopping-cart"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="modal" data-target="#exampleModalLong" href="#">Đăng ký/Đăng nhập</a>
                        </li>
                        <li class="nav-item nav-item-menu-up">
                            <img src="image/vietnamese.png" alt="">
                        </li>
                    </ul>
                </div>
                <nav class="navbar navbar-expand-lg navbar-light bg-light list_menuheader">
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item">
                                <a class="clickmenuhead nav-link active" href="/">TRANG CHỦ</a>
                            </li>
                            <li class="nav-item">
                                <a class="clickmenuhead nav-link" href="/gioithieu">GIỚI THIỆU</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link clickmenuhead dropdown-toggle" href="#" id="navbarDropdown"
                                    role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    SẢN PHẨM
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                   @foreach ($sanpham as $item)
                                   
                                   <a class="dropdown-item" href="san-pham{{$item->id}}">{{$item->name}}</a>   
                                   @endforeach
                                        
                                      
                                    
                                    
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="clickmenuhead nav-link" href="/tintuc">TIN TỨC</a>
                            </li>
                            <li class="nav-item">
                                <a class="clickmenuhead nav-link" href="/lienhe">LIÊN HỆ</a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </div>

</nav>
