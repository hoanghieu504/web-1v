@extends('frontend.master.thanhtoan')
@section('title','Thanh Toán')

@section('thanhtoan')
<section class="container tttt_tabtt_main">
<form action="" method="POST">@csrf
    <div class="row">
        <h2 style="margin-bottom: 20px;" class="w-100 text-center">Thông tin thanh toán</h2>
        <div class="tttt_tabtt col-12 col-sm-12 col-md-6">
            <form class="was-validated">
                <h5>Thông tin người mua</h5>
                <div style="padding-left: 15px;">
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Họ và tên:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="text" class="form-control" id="uname" placeholder="Nhập tên của bạn" name="uname" required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="pwd">Chức danh:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="text" class="form-control" id="pwd" placeholder="Ví dụ: Giám đốc/Kế toán trưởng" name="cder" required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Số điện thoại:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="number" class="form-control" id="phoner" placeholder="Nhập số điện thoại của bạn" name="phoner"
                                        required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="pwd">Email:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="email" class="form-control" id="emailer" placeholder="Nhập email của bạn" name="emailer" required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="pwd">Mã đại lý:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="text" class="form-control" id="coder" placeholder="Nhập mã đại lý" name="coder" required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                </div>
            </form>
            <div>
                <form class="was-validated">
                    <h5>Thông tin đơn vị</h5>
                    <div style="padding-left: 15px;">
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Mã số thuế:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="text" class="form-control" id="codet" placeholder="Nhập mã số thuế"
                                        name="codet" required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Tên đơn vị:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="text" class="form-control" placeholder="Nhập tên đơn vị của bạn"
                                        required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Địa chỉ:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <input type="text" class="form-control" id="uname"
                                        placeholder="Nhập địa chỉ của bạn" name="uname" required="">
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Tỉnh/Thành phố:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <select class="form-control">
                                        <option value="qg1">Hà Nội</option>
                                    </select>
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Quận/Huyện:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <select class="form-control">
                                        <option value="qg1">Cầu Giấy</option>
                                    </select>
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>
                        <div style="margin-bottom: 5px;">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4">
                                    <label for="uname">Phường/Xã:</label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-8">
                                    <select class="form-control">
                                        <!-- <option value="qg1">Việt Nam</option> -->
                                    </select>
                                    <div class="valid-feedback">Sử dụng được.</div>
                                    <div class="invalid-feedback">Không được để trống.</div>
                                </div>
                            </div>
                        </div>

                    </div>
            </div>
            </form>
            <textarea class="ttrformtt" cols="30" rows="5" placeholder="Nội dung"></textarea>
        </div>
        <div class="col-12 col-sm-12 col-md-6">
            <div class="tttt_ttdh_main">
                <h5>Xác nhận đơn hàng</h5>
                <div class="tttt_ttdh_main_clone">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-7">
                            <p>Gói 2 cơ sở dữ liệu (4 công ty) /1 đơn hàng /30 tháng</p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-2">
                            <p>1 gói</p>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3">
                            <p>2.400.000</p>
                        </div>
                    </div>
                </div>
                <h5>Chọn phương thức thanh toán</h5>
                <div class="tttt_ttdh_main_clone">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio1">Thanh toán sau mua</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio2">Chuyển khoản ngân hàng
                            <br>
                            Số Tk:646456534231
                            <br>
                            Ngân hàng Web88 - chi nhánh Cầu Giấy
                            Công ty Web88
                        </label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio3">Thanh toán qua ví</label>
                    </div>
                </div>
            </div>

            <div class="w-100 text-right">
                <button type="submit" class="btn btn-primary mr-auto">Thanh toán</button>
            </div>
        </div>
    </div>
</form>
    </div>
</section>
@endsection
