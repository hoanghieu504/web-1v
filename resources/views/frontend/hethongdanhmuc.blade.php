<section class="container htdm_ctn">
    <h1 class="text-center h1_center">Hệ THỐNG DANH MỤC</h1>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12">
            <div class="card mb-3">
                <div class="row no-gutters">
                    <div class="htdm_image col-md-2">
                        <img src="image/khachhang1.jpg" class="card-img" alt="...">
                    </div>
                    <div class="col-md-10">
                        <div class="card-body">
                            <h5 class="card-title"><a href="##">Giám đốc Công ty Luật Trường Sơn nhận xét về SAG</a>
                            </h5>
                            <p class="card-text">Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp
                                chủ doanh nghiệp vốn không có chuyên môn sâu về kế toán – tài
                                chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính
                                của doanh nghiệp để đưa ra những
                                quyết sách phù hợp trong ngắn hạn và dài hạn.</p>
                            <div class="maintintuccard_btn">
                                <a href="##">
                                    <button type="button" class="btn btn-primary">Xem thêm</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

          
        </div>
    </div>
</section>
