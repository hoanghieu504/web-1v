@extends('frontend.master.master')
@section('title','Sản phẩm')
{{--  @section('sanpham','active')  --}}
@section('content')
<section class="mainsp">
    <div class="container">
        <div class="col-12 col-sm-12 col-md-12 col-xl-12 col-lg-12 col-xs-12">
            <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="home" aria-selected="true">Giới Thiệu</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tinhnang" role="tab"
                        aria-controls="profile" aria-selected="false">Tính Năng</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#baogia" role="tab"
                        aria-controls="contact" aria-selected="false">Báo Giá</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#dowload" role="tab"
                        aria-controls="contact" aria-selected="false">Download</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/hdsd">Hướng dẫn sử dụng</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="container">

                        <section class="container maingioithieu">
                            <h1 class="text-center">GIỚI THIỆU</h1>
                            <p>Kế toán tổng hợp</p>
                            <ul>
                                <li>Phần mềm kế toán 1V là một sản phẩm của công ty Giải pháp Phần mềm SAG. Sản phẩm
                                    được nghiên cứu và thiết kế bởi đội ngũ chuyên gia, nhân sự giàu kinh nghiệm. Sản
                                    phẩm được xây dựng trên nền tảng am hiểu sâu sắc các
                                    bài toán và vấn đề đặt ra của các doanh nghiệp trên nhiều lĩnh vực, giúp doanh
                                    nghiệp có được một công cụ thông minh, đảm bảo hệ thống kế toán – tài chính vận hành
                                    thông suốt, hiệu quả, tối ưu lợi nhuận cho chủ
                                    doanh nghiệp. Với nhiều ưu điểm vượt trội, phần mềm 1V đã trở thành một trong những
                                    sản phẩm được thị trường tin dùng nhất hiện nay. Dưới đây là những đặc tính khác
                                    biệt giúp phần mềm 1V chinh phục gần 2000 doanh
                                    nghiệp nhỏ và vừa trong 12 năm qua: Sản phẩm đáp ứng nhu cầu 2 trong 1 của doanh
                                    nghiệp: Giải pháp tích hợp phần mềm bán hàng ngay trong phần mềm kế toán, giúp doanh
                                    nghiệp tiết kiệm chi phí vận hành, dễ dàng cập
                                    nhật và phân tích tình hình tài chính, bán hàng.</li>
                                <li>1V có thể giải quyết được từng vấn đề đặc thù của từng doanh nghiệp riêng biệt, bởi
                                    sản phẩm có khả năng điều chỉnh linh hoạt theo thực tế lĩnh vực kinh doanh của mỗi
                                    đơn vị. Đây là điều mà không phải phần mềm kế toán
                                    hiện có trên thị trường có thể làm được.</li>
                                <li>Khả năng bảo mật cao, giao diện phần mềm thân thiện và dễ sử dụng với ngôn ngữ tiếng
                                    việt, cho phép cập nhật dữ liệu linh hoạt.</li>
                                <li>Tính chính xác cao: việc tính toán các số liệu rất chính xác và ít xảy ra các sai
                                    sót bất thường, khiến bộ phận kế toán hoàn toàn yên tâm khi sử dụng.</li>
                                <li>Hệ thống bám sát chế độ kế toán, các mẫu biểu chứng từ và sổ sách kế toán luôn tuân
                                    thủ chế độ kế toán hiện hành. Hệ thống cung cấp các báo cáo đa dạng đáp ứng nhiều
                                    nhu cầu quản lý của doanh nghiệp.</li>
                                <li>Chế độ bảo trì: SAG Software cam kết việc bảo hành, bảo trì phần mềm 24/7, luôn
                                    nhanh chóng và kịp thời, nhờ đó mà doanh nghiệp của bạn có thể khắc phục các sự cố
                                    trong một thời gian nhanh nhất.</li>


                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione, illo?</p>
                                <address>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius, deleniti?</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, dolor nulla
                                        voluptatum quidem reiciendis eos maxime earum beatae dolore possimus?</p>
                                    <p>Tổng Đài CSKH : 1900 2658</p>
                                    <p>WEBSITE : <a href="#">https://1v.com.vn</a></p>
                                    <li>Phần mềm kế toán 1V có giá thành phù hợp với mọi doanh nghiệp, đặc biệt là các
                                        doanh nghiệp vừa và nhỏ. Đây
                                        là một ưu
                                        điểm lớn của phần mềm kế toán 1V.</li>
                                    <li>Sản phẩm được hậu thuẫn và đồng hành bởi đội ngũ chuyên gia, nhân sự giàu kinh
                                        nghiệm của tổ hợp dịch vụ Kế
                                        toán & Thuế
                                        SAG.</li>



                            </ul>
                            <p>HÃY NHẤC MÁY ĐT VÀ GỌI NGAY CHO CHÚNG TÔI ĐỂ ĐƯỢC TƯ VẤN MIỄN PHÍ</p>
                            <p>CÔNG TY TNHH GIẢI PHÁP PHẦN MỀM SAG</p>
                            <p>Địa chỉ: <span>Nhà 27-28/TT23, khu đô thị Văn Phú, phường Phú La, quận Hà Đông, thành phố
                                    Hà Nội</span></p>
                            <p>Tổng đài CSKH: <span>1900 2658</span></p>
                            <p><a href="Websile: http://1v.com.vn">Websile: http://1v.com.vn</a></p>
                            <hr>
                        </section>
                        </address>
                    </div>
                </div>
                <div class="tab-pane fade" id="tinhnang" role="tabpanel" aria-labelledby="tinhnang-tab">
                    <div class="container">

                        <section class="container maingioithieu">
                            <h1 class="text-center">TÍNH NĂNG</h1>
                            <p>Kế toán tổng hợp</p>
                            <ul>
                                <li>Phần mềm kế toán 1V là một sản phẩm của công ty Giải pháp Phần mềm SAG. Sản phẩm
                                    được nghiên cứu và thiết kế bởi đội ngũ chuyên gia, nhân sự giàu kinh nghiệm. Sản
                                    phẩm được xây dựng trên nền tảng am hiểu sâu sắc các
                                    bài toán và vấn đề đặt ra của các doanh nghiệp trên nhiều lĩnh vực, giúp doanh
                                    nghiệp có được một công cụ thông minh, đảm bảo hệ thống kế toán – tài chính vận hành
                                    thông suốt, hiệu quả, tối ưu lợi nhuận cho chủ
                                    doanh nghiệp. Với nhiều ưu điểm vượt trội, phần mềm 1V đã trở thành một trong những
                                    sản phẩm được thị trường tin dùng nhất hiện nay. Dưới đây là những đặc tính khác
                                    biệt giúp phần mềm 1V chinh phục gần 2000 doanh
                                    nghiệp nhỏ và vừa trong 12 năm qua: Sản phẩm đáp ứng nhu cầu 2 trong 1 của doanh
                                    nghiệp: Giải pháp tích hợp phần mềm bán hàng ngay trong phần mềm kế toán, giúp doanh
                                    nghiệp tiết kiệm chi phí vận hành, dễ dàng cập
                                    nhật và phân tích tình hình tài chính, bán hàng.</li>
                                <li>1V có thể giải quyết được từng vấn đề đặc thù của từng doanh nghiệp riêng biệt, bởi
                                    sản phẩm có khả năng điều chỉnh linh hoạt theo thực tế lĩnh vực kinh doanh của mỗi
                                    đơn vị. Đây là điều mà không phải phần mềm kế toán
                                    hiện có trên thị trường có thể làm được.</li>
                                <li>Khả năng bảo mật cao, giao diện phần mềm thân thiện và dễ sử dụng với ngôn ngữ tiếng
                                    việt, cho phép cập nhật dữ liệu linh hoạt.</li>
                                <li>Tính chính xác cao: việc tính toán các số liệu rất chính xác và ít xảy ra các sai
                                    sót bất thường, khiến bộ phận kế toán hoàn toàn yên tâm khi sử dụng.</li>
                                <li>Hệ thống bám sát chế độ kế toán, các mẫu biểu chứng từ và sổ sách kế toán luôn tuân
                                    thủ chế độ kế toán hiện hành. Hệ thống cung cấp các báo cáo đa dạng đáp ứng nhiều
                                    nhu cầu quản lý của doanh nghiệp.</li>
                                <li>Chế độ bảo trì: SAG Software cam kết việc bảo hành, bảo trì phần mềm 24/7, luôn
                                    nhanh chóng và kịp thời, nhờ đó mà doanh nghiệp của bạn có thể khắc phục các sự cố
                                    trong một thời gian nhanh nhất.</li>


                                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ratione, illo?</p>
                                <address>
                                    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eius, deleniti?</p>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, dolor nulla
                                        voluptatum quidem reiciendis eos maxime earum beatae dolore possimus?</p>
                                    <p>Tổng Đài CSKH : 1900 2658</p>
                                    <p>WEBSITE : <a href="#">https://1v.com.vn</a></p>
                                    <li>Phần mềm kế toán 1V có giá thành phù hợp với mọi doanh nghiệp, đặc biệt là các
                                        doanh nghiệp vừa và nhỏ. Đây
                                        là một ưu
                                        điểm lớn của phần mềm kế toán 1V.</li>
                                    <li>Sản phẩm được hậu thuẫn và đồng hành bởi đội ngũ chuyên gia, nhân sự giàu kinh
                                        nghiệm của tổ hợp dịch vụ Kế
                                        toán & Thuế
                                        SAG.</li>



                            </ul>
                            <p>HÃY NHẤC MÁY ĐT VÀ GỌI NGAY CHO CHÚNG TÔI ĐỂ ĐƯỢC TƯ VẤN MIỄN PHÍ</p>
                            <p>CÔNG TY TNHH GIẢI PHÁP PHẦN MỀM SAG</p>
                            <p>Địa chỉ: <span>Nhà 27-28/TT23, khu đô thị Văn Phú, phường Phú La, quận Hà Đông, thành phố
                                    Hà Nội</span></p>
                            <p>Tổng đài CSKH: <span>1900 2658</span></p>
                            <p><a href="Websile: http://1v.com.vn">Websile: http://1v.com.vn</a></p>
                            <hr>
                        </section>
                    </div>
                </div>
                {{--  báo giá  --}}
                <div class="tab-pane fade" id="baogia" role="tabpanel" aria-labelledby="baogia-tab">
                    <main class="py-3">
                        <h2 class="w-100 text-center">BÁO GIÁ SẢN PHẨM</h2>
                        <div class="w-100 container">
                            <div class="row">
                                <div class="list_bgsp col-12 col-sm-12 col-md-4">
                                    <div class="card" style="width: 20rem; margin: 0px auto;">
                                        <div class="card-body">
                                            <h5 class="card-title">Gói 2 cơ sở dữ liệu (4 công ty) / 1 đơn hàng / 12
                                                tháng
                                            </h5>
                                            <p class="w-100 text-center">1.200.00VND</p>
                                            <hr>
                                            <p class="w-100 text-center">Không giới hạn</p>
                                            <hr>
                                            <p class="card-text">Gồm 16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                Công việc, Tổng hợp,
                                                Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lương
                                            </p>
                                            <div class="list_btn_cards">
                                                <a href="#thongtinchitietspo" class="click_showctsp btn btn-primary">Đặt
                                                    mua</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list_bgsp active col-12 col-sm-12 col-md-4">
                                    <div class="card" style="width: 20rem; margin: 0px auto;">
                                        <div class="card-body">
                                            <h5 class="card-title">Gói 2 cơ sở dữ liệu (4 công ty) /1 đơn hàng
                                                /30 tháng
                                            </h5>
                                            <p class="w-100 text-center">2.400.00VND</p>
                                            <hr>
                                            <p class="w-100 text-center">Không giới hạn</p>
                                            <hr>
                                            <p class="card-text">Gồm 16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu, Công việc, Tổng
                                                hợp,
                                                Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lương
                                            </p>
                                            <div class="list_btn_cards">
                                                <a href="#thongtinchitietspo" class="click_showctsp btn btn-primary">Đặt
                                                    mua</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="list_bgsp col-12 col-sm-12 col-md-4">
                                    <div class="card" style="width: 20rem; margin: 0px auto;">
                                        <div class="card-body">
                                            <h5 class="card-title">Gói 1 cơ sở dữ liệu (1 công ty) / 1 đơn hàng / 30
                                                tháng
                                            </h5>
                                            <p class="w-100 text-center">4.800.00VND</p>
                                            <hr>
                                            <p class="w-100 text-center">Không giới hạn</p>
                                            <hr>
                                            <p class="card-text">Gồm 16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                Công việc, Tổng hợp,
                                                Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lương
                                            </p>
                                            <div class="list_btn_cards">
                                                <a href="#thongtinchitietspo" class="click_showctsp btn btn-primary">Đặt
                                                    mua</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="thongtinchitietspo">
                            <section class="ctsp_clone_main">
                                <div class="card mb-3">
                                    <div class="row no-gutters">
                                        <div class="col-md-4 text-center">
                                            <img src="image/logo1v.png" class="ctsp_images card-img mt-auto mb-auto"
                                                alt="...">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">Gói 2 cơ sở dữ liệu (4 công ty) /1 đơn hàng
                                                    /12 tháng</h5>
                                                <p>Giá: <b>1.200.00 VNĐ</b></p>
                                                <p class="ctsp_text card-text">Gồm 16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân
                                                    hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lươngGồm
                                                    16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lươngGồm
                                                    16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lương
                                                </p>
                                                <div class="text-right">
                                                    <a href="thanhtoan.php" class="btn btn-primary">Thanh toán</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#chitiet"
                                                class="nav-link active">Chi tiết</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="chitiet" class="tab-pane fade in active show">
                                            <div class="maingioithieu">
                                                <h1 class="text-center">THÔNG TIN CHI TIẾT</h1>
                                                <p>Kế toán tổng hợp</p>
                                                <ul>
                                                    <li>Phần mềm kế toán 1V là một sản phẩm của công ty Giải pháp Phần
                                                        mềm SAG.
                                                        Sản
                                                        phẩm được nghiên cứu và thiết kế bởi đội ngũ chuyên gia, nhân sự
                                                        giàu
                                                        kinh
                                                        nghiệm. Sản phẩm được xây dựng trên nền tảng am hiểu sâu sắc các
                                                        bài toán và vấn đề đặt ra của các doanh nghiệp trên nhiều lĩnh
                                                        vực, giúp
                                                        doanh nghiệp có được một công cụ thông minh, đảm bảo hệ thống kế
                                                        toán –
                                                        tài
                                                        chính vận hành thông suốt, hiệu quả, tối ưu lợi nhuận cho chủ
                                                        doanh nghiệp. Với nhiều ưu điểm vượt trội, phần mềm 1V đã trở
                                                        thành một
                                                        trong những sản phẩm được thị trường tin dùng nhất hiện nay.
                                                        Dưới đây là
                                                        những đặc tính khác biệt giúp phần mềm 1V chinh phục gần 2000
                                                        doanh
                                                        nghiệp nhỏ và vừa trong 12 năm qua: Sản phẩm đáp ứng nhu cầu 2
                                                        trong 1
                                                        của
                                                        doanh nghiệp: Giải pháp tích hợp phần mềm bán hàng ngay trong
                                                        phần mềm
                                                        kế
                                                        toán, giúp doanh nghiệp tiết kiệm chi phí vận hành, dễ dàng cập
                                                        nhật và phân tích tình hình tài chính, bán hàng.</li>
                                                    <li>1V có thể giải quyết được từng vấn đề đặc thù của từng doanh
                                                        nghiệp
                                                        riêng
                                                        biệt, bởi sản phẩm có khả năng điều chỉnh linh hoạt theo thực tế
                                                        lĩnh
                                                        vực
                                                        kinh doanh của mỗi đơn vị. Đây là điều mà không phải phần mềm kế
                                                        toán
                                                        hiện có trên thị trường có thể làm được.</li>
                                                    <li>Khả năng bảo mật cao, giao diện phần mềm thân thiện và dễ sử
                                                        dụng với
                                                        ngôn
                                                        ngữ tiếng việt, cho phép cập nhật dữ liệu linh hoạt.</li>
                                                    <li>Tính chính xác cao: việc tính toán các số liệu rất chính xác và
                                                        ít xảy
                                                        ra
                                                        các sai sót bất thường, khiến bộ phận kế toán hoàn toàn yên tâm
                                                        khi sử
                                                        dụng.
                                                    </li>
                                                    <li>Hệ thống bám sát chế độ kế toán, các mẫu biểu chứng từ và sổ
                                                        sách kế
                                                        toán
                                                        luôn tuân thủ chế độ kế toán hiện hành. Hệ thống cung cấp các
                                                        báo cáo đa
                                                        dạng đáp ứng nhiều nhu cầu quản lý của doanh nghiệp.</li>
                                                    <li>Chế độ bảo trì: SAG Software cam kết việc bảo hành, bảo trì phần
                                                        mềm
                                                        24/7,
                                                        luôn nhanh chóng và kịp thời, nhờ đó mà doanh nghiệp của bạn có
                                                        thể khắc
                                                        phục các sự cố trong một thời gian nhanh nhất.</li>


                                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                                        Ratione, illo?
                                                    </p>
                                                    <address>
                                                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                                            Eius,
                                                            deleniti?
                                                        </p>
                                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Ipsa, dolor
                                                            nulla voluptatum quidem reiciendis eos maxime earum beatae
                                                            dolore
                                                            possimus?</p>
                                                        <p>Tổng Đài CSKH : 1900 2658</p>
                                                        <p>WEBSITE : <a href="#">https://1v.com.vn</a></p>
                                                        <li>Phần mềm kế toán 1V có giá thành phù hợp với mọi doanh
                                                            nghiệp, đặc
                                                            biệt
                                                            là các doanh nghiệp vừa và nhỏ. Đây
                                                            là một ưu
                                                            điểm lớn của phần mềm kế toán 1V.</li>
                                                        <li>Sản phẩm được hậu thuẫn và đồng hành bởi đội ngũ chuyên gia,
                                                            nhân sự
                                                            giàu kinh nghiệm của tổ hợp dịch vụ Kế
                                                            toán & Thuế
                                                            SAG.</li>



                                                </ul>
                                                <p>HÃY NHẤC MÁY ĐT VÀ GỌI NGAY CHO CHÚNG TÔI ĐỂ ĐƯỢC TƯ VẤN MIỄN PHÍ</p>
                                                <p>CÔNG TY TNHH GIẢI PHÁP PHẦN MỀM SAG</p>
                                                <p>Địa chỉ: <span>Nhà 27-28/TT23, khu đô thị Văn Phú, phường Phú La,
                                                        quận Hà
                                                        Đông,
                                                        thành phố Hà Nội</span></p>
                                                <p>Tổng đài CSKH: <span>1900 2658</span></p>
                                                <p><a href="Websile: http://1v.com.vn">Websile: http://1v.com.vn</a></p>
                                                <hr>
                                            </div>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <div>
                                                <p>none</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="ctsp_clone_main active">
                                <div class="card mb-3">
                                    <div class="row no-gutters">
                                        <div class="col-md-4 text-center">
                                            <img src="image/logo1v.png" class="ctsp_images card-img mt-auto mb-auto"
                                                alt="...">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">Gói 2 cơ sở dữ liệu (4 công ty) /1 đơn hàng
                                                    /30 tháng</h5>
                                                <p>Giá: <b>2.400.00 VNĐ</b></p>
                                                <p class="ctsp_text card-text">Gồm 16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân
                                                    hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lươngGồm
                                                    16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lươngGồm
                                                    16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lương
                                                </p>
                                                <div class="text-right">
                                                    <a href="thanhtoan.php" class="btn btn-primary">Thanh toán</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#chitiet"
                                                class="nav-link active">Chi tiết</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="chitiet" class="tab-pane fade in active show">
                                            <div class="maingioithieu">
                                                <h1 class="text-center">THÔNG TIN CHI TIẾT</h1>
                                                <p>Kế toán tổng hợp</p>
                                                <ul>
                                                    <li>Phần mềm kế toán 1V là một sản phẩm của công ty Giải pháp Phần
                                                        mềm SAG.
                                                        Sản
                                                        phẩm được nghiên cứu và thiết kế bởi đội ngũ chuyên gia, nhân sự
                                                        giàu
                                                        kinh
                                                        nghiệm. Sản phẩm được xây dựng trên nền tảng am hiểu sâu sắc các
                                                        bài toán và vấn đề đặt ra của các doanh nghiệp trên nhiều lĩnh
                                                        vực, giúp
                                                        doanh nghiệp có được một công cụ thông minh, đảm bảo hệ thống kế
                                                        toán –
                                                        tài
                                                        chính vận hành thông suốt, hiệu quả, tối ưu lợi nhuận cho chủ
                                                        doanh nghiệp. Với nhiều ưu điểm vượt trội, phần mềm 1V đã trở
                                                        thành một
                                                        trong những sản phẩm được thị trường tin dùng nhất hiện nay.
                                                        Dưới đây là
                                                        những đặc tính khác biệt giúp phần mềm 1V chinh phục gần 2000
                                                        doanh
                                                        nghiệp nhỏ và vừa trong 12 năm qua: Sản phẩm đáp ứng nhu cầu 2
                                                        trong 1
                                                        của
                                                        doanh nghiệp: Giải pháp tích hợp phần mềm bán hàng ngay trong
                                                        phần mềm
                                                        kế
                                                        toán, giúp doanh nghiệp tiết kiệm chi phí vận hành, dễ dàng cập
                                                        nhật và phân tích tình hình tài chính, bán hàng.</li>
                                                    <li>1V có thể giải quyết được từng vấn đề đặc thù của từng doanh
                                                        nghiệp
                                                        riêng
                                                        biệt, bởi sản phẩm có khả năng điều chỉnh linh hoạt theo thực tế
                                                        lĩnh
                                                        vực
                                                        kinh doanh của mỗi đơn vị. Đây là điều mà không phải phần mềm kế
                                                        toán
                                                        hiện có trên thị trường có thể làm được.</li>
                                                    <li>Khả năng bảo mật cao, giao diện phần mềm thân thiện và dễ sử
                                                        dụng với
                                                        ngôn
                                                        ngữ tiếng việt, cho phép cập nhật dữ liệu linh hoạt.</li>
                                                    <li>Tính chính xác cao: việc tính toán các số liệu rất chính xác và
                                                        ít xảy
                                                        ra
                                                        các sai sót bất thường, khiến bộ phận kế toán hoàn toàn yên tâm
                                                        khi sử
                                                        dụng.
                                                    </li>
                                                    <li>Hệ thống bám sát chế độ kế toán, các mẫu biểu chứng từ và sổ
                                                        sách kế
                                                        toán
                                                        luôn tuân thủ chế độ kế toán hiện hành. Hệ thống cung cấp các
                                                        báo cáo đa
                                                        dạng đáp ứng nhiều nhu cầu quản lý của doanh nghiệp.</li>
                                                    <li>Chế độ bảo trì: SAG Software cam kết việc bảo hành, bảo trì phần
                                                        mềm
                                                        24/7,
                                                        luôn nhanh chóng và kịp thời, nhờ đó mà doanh nghiệp của bạn có
                                                        thể khắc
                                                        phục các sự cố trong một thời gian nhanh nhất.</li>


                                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                                        Ratione, illo?
                                                    </p>
                                                    <address>
                                                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                                            Eius,
                                                            deleniti?
                                                        </p>
                                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Ipsa, dolor
                                                            nulla voluptatum quidem reiciendis eos maxime earum beatae
                                                            dolore
                                                            possimus?</p>
                                                        <p>Tổng Đài CSKH : 1900 2658</p>
                                                        <p>WEBSITE : <a href="#">https://1v.com.vn</a></p>
                                                        <li>Phần mềm kế toán 1V có giá thành phù hợp với mọi doanh
                                                            nghiệp, đặc
                                                            biệt
                                                            là các doanh nghiệp vừa và nhỏ. Đây
                                                            là một ưu
                                                            điểm lớn của phần mềm kế toán 1V.</li>
                                                        <li>Sản phẩm được hậu thuẫn và đồng hành bởi đội ngũ chuyên gia,
                                                            nhân sự
                                                            giàu kinh nghiệm của tổ hợp dịch vụ Kế
                                                            toán & Thuế
                                                            SAG.</li>



                                                </ul>
                                                <p>HÃY NHẤC MÁY ĐT VÀ GỌI NGAY CHO CHÚNG TÔI ĐỂ ĐƯỢC TƯ VẤN MIỄN PHÍ</p>
                                                <p>CÔNG TY TNHH GIẢI PHÁP PHẦN MỀM SAG</p>
                                                <p>Địa chỉ: <span>Nhà 27-28/TT23, khu đô thị Văn Phú, phường Phú La,
                                                        quận Hà
                                                        Đông,
                                                        thành phố Hà Nội</span></p>
                                                <p>Tổng đài CSKH: <span>1900 2658</span></p>
                                                <p><a href="Websile: http://1v.com.vn">Websile: http://1v.com.vn</a></p>
                                                <hr>
                                            </div>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <div>
                                                <p>none</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <section class="ctsp_clone_main">
                                <div class="card mb-3">
                                    <div class="row no-gutters">
                                        <div class="col-md-4 text-center">
                                            <img src="image/logo1v.png" class="ctsp_images card-img mt-auto mb-auto"
                                                alt="...">
                                        </div>
                                        <div class="col-md-8">
                                            <div class="card-body">
                                                <h5 class="card-title">Gói 1 cơ sở dữ liệu (1 công ty) /1 đơn hàng
                                                    /30 tháng</h5>
                                                <p>Giá: <b>4.800.00 VNĐ</b></p>
                                                <p class="ctsp_text card-text">Gồm 16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân
                                                    hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lươngGồm
                                                    16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lươngGồm
                                                    16 Nghiệp vụ: Quỹ, Thủ quỹ, Ngân hàng, Mua
                                                    hàng, Bán hàng, Quản lý hóa đơn, Thuế, Tài liệu,
                                                    Công việc, Tổng hợp,
                                                    Kho, Thủ kho, Công cụ dụng cụ, Giá thành, Tài sản, Tiền lương
                                                </p>
                                                <div class="text-right">
                                                    <a href="thanhtoan.php" class="btn btn-primary">Thanh toán</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#chitiet"
                                                class="nav-link active">Chi tiết</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="chitiet" class="tab-pane fade in active show">
                                            <div class="maingioithieu">
                                                <h1 class="text-center">THÔNG TIN CHI TIẾT</h1>
                                                <p>Kế toán tổng hợp</p>
                                                <ul>
                                                    <li>Phần mềm kế toán 1V là một sản phẩm của công ty Giải pháp Phần
                                                        mềm SAG.
                                                        Sản
                                                        phẩm được nghiên cứu và thiết kế bởi đội ngũ chuyên gia, nhân sự
                                                        giàu
                                                        kinh
                                                        nghiệm. Sản phẩm được xây dựng trên nền tảng am hiểu sâu sắc các
                                                        bài toán và vấn đề đặt ra của các doanh nghiệp trên nhiều lĩnh
                                                        vực, giúp
                                                        doanh nghiệp có được một công cụ thông minh, đảm bảo hệ thống kế
                                                        toán –
                                                        tài
                                                        chính vận hành thông suốt, hiệu quả, tối ưu lợi nhuận cho chủ
                                                        doanh nghiệp. Với nhiều ưu điểm vượt trội, phần mềm 1V đã trở
                                                        thành một
                                                        trong những sản phẩm được thị trường tin dùng nhất hiện nay.
                                                        Dưới đây là
                                                        những đặc tính khác biệt giúp phần mềm 1V chinh phục gần 2000
                                                        doanh
                                                        nghiệp nhỏ và vừa trong 12 năm qua: Sản phẩm đáp ứng nhu cầu 2
                                                        trong 1
                                                        của
                                                        doanh nghiệp: Giải pháp tích hợp phần mềm bán hàng ngay trong
                                                        phần mềm
                                                        kế
                                                        toán, giúp doanh nghiệp tiết kiệm chi phí vận hành, dễ dàng cập
                                                        nhật và phân tích tình hình tài chính, bán hàng.</li>
                                                    <li>1V có thể giải quyết được từng vấn đề đặc thù của từng doanh
                                                        nghiệp
                                                        riêng
                                                        biệt, bởi sản phẩm có khả năng điều chỉnh linh hoạt theo thực tế
                                                        lĩnh
                                                        vực
                                                        kinh doanh của mỗi đơn vị. Đây là điều mà không phải phần mềm kế
                                                        toán
                                                        hiện có trên thị trường có thể làm được.</li>
                                                    <li>Khả năng bảo mật cao, giao diện phần mềm thân thiện và dễ sử
                                                        dụng với
                                                        ngôn
                                                        ngữ tiếng việt, cho phép cập nhật dữ liệu linh hoạt.</li>
                                                    <li>Tính chính xác cao: việc tính toán các số liệu rất chính xác và
                                                        ít xảy
                                                        ra
                                                        các sai sót bất thường, khiến bộ phận kế toán hoàn toàn yên tâm
                                                        khi sử
                                                        dụng.
                                                    </li>
                                                    <li>Hệ thống bám sát chế độ kế toán, các mẫu biểu chứng từ và sổ
                                                        sách kế
                                                        toán
                                                        luôn tuân thủ chế độ kế toán hiện hành. Hệ thống cung cấp các
                                                        báo cáo đa
                                                        dạng đáp ứng nhiều nhu cầu quản lý của doanh nghiệp.</li>
                                                    <li>Chế độ bảo trì: SAG Software cam kết việc bảo hành, bảo trì phần
                                                        mềm
                                                        24/7,
                                                        luôn nhanh chóng và kịp thời, nhờ đó mà doanh nghiệp của bạn có
                                                        thể khắc
                                                        phục các sự cố trong một thời gian nhanh nhất.</li>


                                                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                                                        Ratione, illo?
                                                    </p>
                                                    <address>
                                                        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                                                            Eius,
                                                            deleniti?
                                                        </p>
                                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                                            Ipsa, dolor
                                                            nulla voluptatum quidem reiciendis eos maxime earum beatae
                                                            dolore
                                                            possimus?</p>
                                                        <p>Tổng Đài CSKH : 1900 2658</p>
                                                        <p>WEBSITE : <a href="#">https://1v.com.vn</a></p>
                                                        <li>Phần mềm kế toán 1V có giá thành phù hợp với mọi doanh
                                                            nghiệp, đặc
                                                            biệt
                                                            là các doanh nghiệp vừa và nhỏ. Đây
                                                            là một ưu
                                                            điểm lớn của phần mềm kế toán 1V.</li>
                                                        <li>Sản phẩm được hậu thuẫn và đồng hành bởi đội ngũ chuyên gia,
                                                            nhân sự
                                                            giàu kinh nghiệm của tổ hợp dịch vụ Kế
                                                            toán & Thuế
                                                            SAG.</li>



                                                </ul>
                                                <p>HÃY NHẤC MÁY ĐT VÀ GỌI NGAY CHO CHÚNG TÔI ĐỂ ĐƯỢC TƯ VẤN MIỄN PHÍ</p>
                                                <p>CÔNG TY TNHH GIẢI PHÁP PHẦN MỀM SAG</p>
                                                <p>Địa chỉ: <span>Nhà 27-28/TT23, khu đô thị Văn Phú, phường Phú La,
                                                        quận Hà
                                                        Đông,
                                                        thành phố Hà Nội</span></p>
                                                <p>Tổng đài CSKH: <span>1900 2658</span></p>
                                                <p><a href="Websile: http://1v.com.vn">Websile: http://1v.com.vn</a></p>
                                                <hr>
                                            </div>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <div>
                                                <p>none</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </main>
                </div>
                <div class="tab-pane fade active show" id="dowload" role="tabpanel" aria-labelledby="dowload-tab">
                    <div class="container ">
                        <h5 class="text-primary py-5">Dowload phiên bản mới nhất</h5>
                        <div class="row">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <img src="image/logo1v.png" alt="" class="img-fluid pl-3">
                                <br>
                                <h1 class="text-danger">Phần mềm 1V</h1>
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <h5 class="text-primary">1v 2020</h5>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit excepturi animi,
                                    tempore earum debitis
                                    molestiae nesciunt itaque rerum nostrum? Quidem et tempora quisquam quas hic illum
                                    nostrum ipsum
                                    delectus corporis.</p>
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <button class="btn btn-primary">Download</button>
                            </div>
                        </div>
                        <h5 class="text-primary py-5">Dowload phiên bản cũ</h5>
                        <div class="row">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <img src="image/logo1v.png" alt="" class="img-fluid pl-3">
                                <br>
                                <h1 class="text-danger">Phần mềm 1V</h1>
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <h5 class="text-primary">1v 2019</h5>
                                <p><a href="#">Bộ Cài</a></p>
                                <p><a href="#"> video hướng dẫn</a></p>
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <button class="btn btn-primary">Download</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <img src="image/logo1v.png" alt="" class="img-fluid pl-3">
                                <br>
                                <h1 class="text-danger">Phần mềm 1V</h1>
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <h5 class="text-primary">1v 2018</h5>
                                <p><a href="#">Bộ Cài</a></p>
                                <p><a href="#"> video hướng dẫn</a></p>
                            </div>
                            <div class="col-4 col-sm-4 col-md-4 col-lg-4 col-xs-4 col-xl-4">
                                <button class="btn btn-primary">Download</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade " id="huongdan " role="tabpanel " aria-labelledby="huongdan-tab ">
                    ...
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('script')
$(".clickmenuhead")[0].classList.remove("active");
$(".clickmenuhead")[2].classList.add("active");
@endsection