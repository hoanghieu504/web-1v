<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <base href="{{asset('')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css"
            integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M"
            crossorigin="anonymous" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	<link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
            <link rel="stylesheet" href="frontend/style/stylefooter.css">
            <link rel="stylesheet" href="frontend/style/styleheader.css">
            <link rel="stylesheet" href="frontend/style/styleslideshow.css">
            <link rel="stylesheet" href="frontend/style/stylemaintop.css">
            <link rel="stylesheet" href="frontend/style/styletintucclone.css">
            <link rel="stylesheet" href="frontend/style/style.css">
    </head>
    <body>
        @include("frontend.master.arrowup")
        <header id="header" class="container-fluid">
            @include("frontend.master.menuheader")
            @include("frontend.slideshow")
        </header>

        <main id="mainindex" class="container-fluid">
            <section class="container">
                <h1 class="text-center h1ttclone">TIN TỨC</h1>
                <h3 class="text-center">{{$post->title}}</h3>
                {{--  <div class="ttclone_img"><img class="" src="image/Khachhang2.jpg" alt=""></div>  --}}
                {!!$post->contents!!}
                <h2 class="maintop_h2">BÀI VIẾT LIÊN QUAN</h2>
                <div class="row">
                    @foreach ($postlist as $item)
                        
                   
                    <div class="ttclone_lq card col-12 col-sm-12 col-md-4" style="width: 18rem;">
                        <div><img src="image/{{$item->image}}" class="card-img-top" alt="..."></div>
                        <div class="ttclone_lq_text card-body">
                            <div class="overlay">
                                <h5 class="card-title"><a href="tintuc/{{$item->id}}">{{$item->title}}</a></h5>
                                <p class="card-text"><a href="tintuc/{{$item->id}}">
                                    {!!$item->content!!}
                                </a></p>
                            </div>
                        </div>
                    </div>
                    @endforeach
            
                </div>
            </section>
            
            @include('frontend.mainbutton')
        </main>

        <footer class="container-fluid footerr">
            @include("frontend.master.footer")
        </footer>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
            <script src="frontend/script/script.js"></script>
            <script>
                $(".clickmenuhead")[0].classList.remove("active");
                $(".clickmenuhead")[3].classList.add("active");
            </script>
    </body>

</html>
