<!-- khách hàng của chúng tôi -->
<section class="container">
    <h2 class="maintop_h2">Khách hàng của chúng tôi</h2>
    <div>
        <div class="row">
            <div class="card col-12 col-sm-12 col-md-4 maintop_stcards_clone" style="width: 18rem;">
                <div class="maintop_why_video">
                    <iframe width="100%" height="250px" src="https://www.youtube.com/embed/udYzc66YVug?controls=0"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Giám đốc Công ty Luật Trường Sơn nhận xét về SAG</h5>
                    <p class="card-text mainbt_cards_text">Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp chủ
                        doanh nghiệp vốn
                        không có chuyên môn sâu về kế toán – tài
                        chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính của doanh
                        nghiệp để đưa ra những
                        quyết sách phù hợp trong ngắn hạn và dài hạn.</p>
                </div>
            </div>
            <div class="card col-12 col-sm-12 col-md-4 maintop_stcards_clone " style="width: 18rem;">
                <div class="maintop_why_video">
                    <iframe width="100%" height="250px" src="https://www.youtube.com/embed/udYzc66YVug?controls=0"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Giám đốc Công ty Viettotal nhận xét về dịch vụ kế toán của SAG</h5>
                    <p class="card-text mainbt_cards_text">Sở hữu chuỗi giá trị từ sản xuất, chăn nuôi đến thương mại,
                        việc vận hành chuỗi
                        giá trị sao cho hiệu quả và dòng tiền
                        thông suốt luôn là bài toán đau đầu với chúng tôi. Giải pháp phần mềm 1V đã trở thành công cụ
                        đắc lực giúp chuỗi thực
                        phẩm sạch Orfarm phát triển mạnh mẽ trong thời gian qua, giúp chúng tôi tiết kiệm được 80% chi
                        phí nhân sự và vận hành.</p>
                </div>
            </div>
            <div class="card col-12 col-sm-12 col-md-4 maintop_stcards_clone" style="width: 18rem;">
                <div class="maintop_why_video">
                    <iframe width="100%" height="250px" src="https://www.youtube.com/embed/udYzc66YVug?controls=0"
                        frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                </div>
                <div class="card-body">
                    <h5 class="card-title">Chia sẻ của Giám đốc Chuỗi thực phẩm sạch Orfarm về phần mềm kế toán</h5>
                    <p class="card-text mainbt_cards_text">Đã thử nghiệm nhiều phần mềm khác nhau nhưng chỉ với phần mềm
                        1V, các tính năng
                        và thông số được may đo riêng cho doanh
                        nghiệp và phù hợp với đặc thù ngành, vì vậy, 1V được chúng tôi tin tưởng lựa chọn trong dài hạn.
                        Tốc độ tăng trưởng của
                        Beekids thời gian qua phát triển vượt bậc, một phần nhờ việc ứng dụng phần mềm 1V.</p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- đối tác của chúng tôi -->
<h2 class="maintop_h2">đối tác của chúng tôi</h2>
<section class="container co-operate carousel slide" data-ride="carousel">
    <div id="owl-co-operate" class="owl-carousel owl-theme">
      
        @foreach ($doitac as $item)
            <div class="item"> <img class="img-fluid" src="image/{{$item->image}}" alt=""> </div>
        @endforeach
    </div>
</section>
<!-- liên hệ -->
<section class="container maintop_stgt">
    <div class="overlay">
        <h2>LIÊN HỆ MUA HÀNG</h2>
        <p>
            <span><i class="fas fa-phone-alt"></i>&nbsp;&nbsp;19002658</span>
            <a href="http://1v.com.vn/"><i class="fas fa-globe-asia"></i>&nbsp;&nbsp;http://1v.com.vn/</a></p>
        <div class="mainbt_btn">
            <button type="button" class="btn btn-primary">XEM DEMO</button>
            <button type="button" class="btn btn-warning">DÙNG THỬ MIỄN PHÍ</button>
            <button type="button" class="btn btn-secondary">MUA NGAY</button>
        </div>
    </div>
</section>
