@extends('frontend.master.master')
@section('title','Giỏ Hàng')
@section('content')
<!-- giới thiệu -->
<section class="container maintop_stgt">
    <div class="overlay">
        <h2>SAG</h2>
        <p>SAG tự hào là đơn vị cung cấp dịch vụ tư vấn chuyên nghiệp trong các lĩnh vực: tư vấn thành lập doanh nghiệp,
            tư
            vấn tài
            chính, dịch vụ kế toán – thuế,
            đào tạo thực hành kế toán và cung cấp phần mềm kế toán cho các doanh nghiệp.</p>
    </div>
</section>
<!-- đặc điểm nổi bật -->
<section class="container">
    <h2 class="maintop_h2">ĐẶC ĐIỂM NỔI BẬT CỦA 1V</h2>
    <div>
        <div class="row">
            <div class="card col-12 col-sm-12 col-md-4 maintop_stcards_clone" style="width: 18rem;">
                <img src="image/unnamed.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">ĐƠN GIẢN & DỄ DÙNG</h5>
                    <p class="card-text">Nhân viên bán hàng chỉ mất 15 phút làm quen để bắt đầu bán hàng với phần mềm 1V
                        . Giao diện đơn giản, thân thiện, thông
                        minh giúp bạn triển khai quản lý bán hàng thật dễ dàng và nhanh chóng.</p>
                </div>
            </div>
            <div class="card col-12 col-sm-12 col-md-4 maintop_stcards_clone " style="width: 18rem;">
                <img src="image/team.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">PHÙ HỢP CHO TỪNG NGÀNH HÀNG</h5>
                    <p class="card-text">Cùng với các chuyên gia bán hàng dày kinh nghiệm, chúng tôi nghiên cứu thiết kế
                        phần mềm phù hợp đến hơn 15 ngành hàng
                        dành cho cả bán buôn lẫn bán lẻ.</p>
                </div>
            </div>
            <div class="card col-12 col-sm-12 col-md-4 maintop_stcards_clone" style="width: 18rem;">
                <img src="image/images.png" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">BẢO HÀNH DÀI HẠN</h5>
                    <p class="card-text">Miễn phí cài đặt, phí triển khai, nâng cấp và hỗ trợ. Rẻ hơn một ly trà đá, chỉ
                        từ 5.000 đồng/ngày, bạn đã có thể áp
                        dụng công nghệ vào quản lý cửa hàng.</p>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- nghiệp vụ -->
<section class="container">
    <h2 style="color:red" class="maintop_h2">MỌI NGHIỆP VỤ BẠN CẦN CHÚNG TÔI ĐỀU CÓ</h2>
    <div id="moinghiepvu">
        <h3 class="w-100 text-center">Quỹ</h3>
        <p>Nhân viên bán hàng chỉ mất 15 phút làm quen để bắt đầu bán hàng với phần mềm 1V . Giao diện đơn giản, thân thiện, thông minh giúp bạn triển khai quản lý bán hàng thật dễ dàng và nhanh chóng.</p>
    </div>
    <div class="row maintop_st_nv">
        <div class="col-12 col-sm-4">
            <ul>
                <li><a class="maintop_st_nv_li_active" href="##"><i class="fas fa-caret-right"></i>Quỹ</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Ngân hàng điện tử</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Mua hàng</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Bán hàng</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Hợp đồng</a></li>
            </ul>
        </div>

        <div class="col-12 col-sm-4">
            <ul>
                <li><a href="##"><i class="fas fa-caret-right"></i>Quản lý hóa đơn</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Hóa đơn điện tử</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Thuế</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Kho</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Công cụ dụng cụ</a></li>
            </ul>
        </div>

        <div class="col-12 col-sm-4">
            <ul>
                <li><a href="##"><i class="fas fa-caret-right"></i>Tài sản cố định</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Tiền lương</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Giá thành</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Tổng hợp</a></li>
                <li><a href="##"><i class="fas fa-caret-right"></i>Ngân sách</a></li>
            </ul>
        </div>

    </div>
</section>
<!-- TẠI SAO BẠN NÊN CHỌN PHẦN MỀM KẾ TOÁN CỦA CHÚNG TÔI -->
<section class="container">
    <h2 class="maintop_h2">TẠI SAO BẠN NÊN CHỌN PHẦN MỀM KẾ TOÁN CỦA CHÚNG TÔI</h2>
    <div>
        <div class="row">
            <div class="col-sm">
                <div class="card mb-3 maintop_st_card_clone">
                    <div class="row no-gutters">
                        <div class="col-md-2 maintop_st_cardimg">
                            <img src="image/25626313-thank-you-sign-icon-customer-service-symbol-red-award-label-with-stars-and-ribbons-vector.jpg"
                                class="card-img" alt="...">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">THƯƠNG HIỆU UY TÍN</h5>
                                <p class="card-text">Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
                                    Tổng cục Thuê ngay trên phần mềm.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3 maintop_st_card_clone">
                    <div class="row no-gutters">
                        <div class="col-md-2 maintop_st_cardimg">
                            <img src="image/lets-go-rocket-icon.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">TIÊN PHONG CÔNG NGHỆ</h5>
                                <p class="card-text">Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
                                    Tổng cục Thuê ngay trên phần mềm.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3 maintop_st_card_clone">
                    <div class="row no-gutters">
                        <div class="col-md-2 maintop_st_cardimg">
                            <img src="image/unnamedt.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">TỐI ƯU HÓA HIỆU QUẢ LÀM VIỆC</h5>
                                <p class="card-text">Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng – Tổng
                                    cục Thuê ngay trên phần mềm.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="card mb-3 maintop_st_card_clone">
                    <div class="row no-gutters">
                        <div class="col-md-2 maintop_st_cardimg">
                            <img src="image/Tư-vấn-Icon.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">TƯ VẤN TẬN TÌNH</h5>
                                <p class="card-text">Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
                                    Tổng cục Thuê ngay trên phần mềm.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3 maintop_st_card_clone">
                    <div class="row no-gutters">
                        <div class="col-md-2 maintop_st_cardimg">
                            <img src="image/icon-thuyet-trinh-400x400.png" class="card-img" alt="...">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">ĐÀO TẠO HỌC ONLINE MIỄN PHÍ</h5>
                                <p class="card-text">Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
                                    Tổng cục Thuê ngay trên phần mềm.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-3 maintop_st_card_clone">
                    <div class="row no-gutters">
                        <div class="col-md-2 maintop_st_cardimg">
                            <img src="image/pngtree-refresh-icon-for-your-project-png-image_4845338.jpg"
                                class="card-img" alt="...">
                        </div>
                        <div class="col-md-10">
                            <div class="card-body">
                                <h5 class="card-title">CẬP NHẬT KỊP THỜI</h5>
                                <p class="card-text">Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
                                    Tổng cục Thuê ngay trên phần mềm.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="maintop_why_video">
            <iframe width="50%" height="315" src="https://www.youtube.com/embed/udYzc66YVug?controls=0" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
        </div>
    </div>
</section>

@endsection