@extends('frontend.master.master')
@section('title','Trang chủ')
@section('content')
<section class="container">
    <h1 class="text-center h1_center">LIÊN HỆ</h1>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-6">
            <p class="ft_lh lh_dc">
                <span>ĐC đăng ký:</span> Số 48 Vạn Phúc Thượng, Tổ 04, cụm 05, P.Cống Vị, Q. Ba Đình, TP. Hà
                Nội. <br>
                <span>ĐC hoạt động:</span> Nhà 27-28/TT23, KTD Văn Phú, P.Phú La, Q.Hà Đông, TP. Hà Nội. <br>
                <span>Mã số thuế:</span> 0108 481 637. <br>
                <span>SĐT:</span> 1900 2658. <br>
                <span>Hotline:</span> 0986.807.407 <br>
                <span>Email:</span> info@sagvietnam.com <br>
                <span>Website:</span> <a href="http://sagvietnam.com/" target="_blank">http://sagvietnam.com/</a>
            </p>
        </div>
        <div class="col-12 col-sm-12 col-md-6 lh_form">
            <h5 class="text-center">Bạn vui lòng để lại thông tin</h5>
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Họ tên</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tên công ty</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Số điện thoại</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tỉnh/TP</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Nội dung</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Gửi</button>
            </form>
        </div>
    </div>
    <div class="lh_image">
        <img src="image/map.png" alt="">
    </div>

</section>
@endsection