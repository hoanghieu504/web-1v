<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Sản Phẩm</title>
       
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css"
            integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M"
            crossorigin="anonymous" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
            <link rel="stylesheet" href="{{ asset('style/stylefooter.css')}}">
            <link rel="stylesheet" href="{{ asset('style/styleheader.css')}}">
            <link rel="stylesheet" href="{{ asset('style/styleslideshow.css')}}">
            <link rel="stylesheet" href="{{ asset('style/stylemaintop.css')}}">
            <link rel="stylesheet" href="{{ asset('style/style.css')}}">
            <link rel="stylesheet" href="{{ asset('style/stylemainsp.css')}}">
    </head>

    <body>
        @include("frontend.master.arrowup")
        <header id="header" class="container-fluid">
            @include("frontend.master.menuheader")
            @include("frontend.slideshow")
           
        </header>
        <main class="container-fluid">
            @include("frontend.sanpham1")
        </main>

        <footer class="container-fluid footerr">
            @include("frontend.master.footer")
        </footer>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <script src="{{ asset('script/script.js')}}"></script>
    <script>
        $(".clickmenuhead")[0].classList.remove("active");
        $(".clickmenuhead")[2].classList.add("active");
    </script>
    </body>

</html>
