@extends('frontend.master.master')
@section('title','Giới thiệu')
@section('gioithieu','active')
@section('content')
<section class="container maingioithieux">
    <h1 class="text-center h1_center">GIỚI THIỆU</h1>
    <p>Kế toán tổng hợp</p>
    <ul>
        <li>Phần mềm kế toán 1V là một sản phẩm của công ty Giải pháp Phần mềm SAG. Sản phẩm được nghiên cứu và thiết kế
            bởi đội ngũ
            chuyên gia, nhân sự giàu kinh nghiệm. Sản phẩm được xây dựng trên nền tảng am hiểu sâu sắc các bài toán và
            vấn đề đặt ra
            của các doanh nghiệp trên nhiều lĩnh vực, giúp doanh nghiệp có được một công cụ thông minh, đảm bảo hệ thống
            kế toán –
            tài chính vận hành thông suốt, hiệu quả, tối ưu lợi nhuận cho chủ doanh nghiệp. Với nhiều ưu điểm vượt trội,
            phần mềm 1V
            đã trở thành một trong những sản phẩm được thị trường tin dùng nhất hiện nay. Dưới đây là những đặc tính
            khác biệt giúp
            phần mềm 1V chinh phục gần 2000 doanh nghiệp nhỏ và vừa trong 12 năm qua: Sản phẩm đáp ứng nhu cầu 2 trong 1
            của doanh
            nghiệp: Giải pháp tích hợp phần mềm bán hàng ngay trong phần mềm kế toán, giúp doanh nghiệp tiết kiệm chi
            phí vận hành,
            dễ dàng cập nhật và phân tích tình hình tài chính, bán hàng.</li>
        <li>1V có thể giải quyết được từng vấn đề đặc thù của từng doanh nghiệp riêng biệt, bởi sản phẩm có khả năng
            điều chỉnh linh
            hoạt theo thực tế lĩnh vực kinh doanh của mỗi đơn vị. Đây là điều mà không phải phần mềm kế toán hiện có
            trên thị trường
            có thể làm được.</li>
        <li>Khả năng bảo mật cao, giao diện phần mềm thân thiện và dễ sử dụng với ngôn ngữ tiếng việt, cho phép cập nhật
            dữ liệu
            linh hoạt.</li>
        <li>Tính chính xác cao: việc tính toán các số liệu rất chính xác và ít xảy ra các sai sót bất thường, khiến bộ
            phận kế toán
            hoàn toàn yên tâm khi sử dụng.</li>
        <li>Hệ thống bám sát chế độ kế toán, các mẫu biểu chứng từ và sổ sách kế toán luôn tuân thủ chế độ kế toán hiện
            hành. Hệ
            thống cung cấp các báo cáo đa dạng đáp ứng nhiều nhu cầu quản lý của doanh nghiệp.</li>
        <li>Chế độ bảo trì: SAG Software cam kết việc bảo hành, bảo trì phần mềm 24/7, luôn nhanh chóng và kịp thời, nhờ
            đó mà doanh
            nghiệp của bạn có thể khắc phục các sự cố trong một thời gian nhanh nhất.</li>
        <li>Phần mềm kế toán 1V có giá thành phù hợp với mọi doanh nghiệp, đặc biệt là các doanh nghiệp vừa và nhỏ. Đây
            là một ưu
            điểm lớn của phần mềm kế toán 1V.</li>
        <li>Sản phẩm được hậu thuẫn và đồng hành bởi đội ngũ chuyên gia, nhân sự giàu kinh nghiệm của tổ hợp dịch vụ Kế
            toán & Thuế
            SAG.</li>



    </ul>
    <p>HÃY NHẤC MÁY ĐT VÀ GỌI NGAY CHO CHÚNG TÔI ĐỂ ĐƯỢC TƯ VẤN MIỄN PHÍ</p>
    <p>CÔNG TY TNHH GIẢI PHÁP PHẦN MỀM SAG</p>
    <p>Địa chỉ: <span>Nhà 27-28/TT23, khu đô thị Văn Phú, phường Phú La, quận Hà Đông, thành phố Hà Nội</span></p>
    <p>Tổng đài CSKH: <span>1900 2658</span></p>
    <p><a href="Websile: http://1v.com.vn">Websile: http://1v.com.vn</a></p>
    <hr>
</section>
<!-- khách hàng của chúng tôi -->

@endsection
@section('script')
@parent
<script>
    $(".clickmenuhead")[0].classList.remove("active");
    $(".clickmenuhead")[1].classList.add("active");
</script>

@endsection
