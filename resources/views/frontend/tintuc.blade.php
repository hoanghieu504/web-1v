@extends('frontend.master.master')
@section('title','Tin tức')

@section('content')
<section class="container">
    <h1 class="text-center h1_center">TIN TỨC</h1>
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8">
            @foreach ($post as $item)
                
                
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <a href="showtintucclone.php"><img src="image/{{$item->image}}" class="card-img" alt="..."></a>
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title"><a href="tintuc/{{$item->id}}">{{$item->title}}</a>
                                </h5>
                                <p class="card-text maintintuccard">
                                    {!!$item->content!!}
                                </p>
                                <div class="maintintuccard_btn">
                                    <a href="tintuc/{{$item->id}}">
                                        <button type="button" class="btn btn-primary">Xem thêm</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="maintintuc_btn">
                <div class="btn-group mr-2" role="group" aria-label="First group">
                   
                    {{ $post->links()}}
                   
                    
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4">
            <div class="maintintuc_gdv">
                <div class="row">
                    <div class="col-12 maintintuc_gdv_title">
                        <p>CÁC GÓI DỊCH VỤ</p>
                    </div>
                </div>
                <div class="maintintuc_link">
                    @foreach ($Product as $item)
                        
                   
                    <p><a href="san-pham1">{{$item->name}}</a></p><br>
                    @endforeach
                </div>
            </div>

            <div class="maintintuc_gdv">
                <div class="row ">
                    <div class="col-12 maintintuc_gdv_title">
                        <p>HƯỚNG DẪN SỬ DỤNG</p>
                    </div>
                </div>
                <div class="maintintuc_link">
                    <p><a href="hdsd/1">Hệ thống danh mục</a></p><br>
                    <p><a href="hdsd/2">Số dư đầu</a></p><br>
                    <p><a href="hdsd/3">Hệ thống chứng từ</a></p><br>
                    <p><a href="hdsd/4">Tiện ích</a></p><br>
                </div>
            </div>
            <div class="maintintuc_gdv">
                <div class="row ">
                    <div class="col-12 maintintuc_gdv_title">
                        <p>DOWNLOAD</p>
                    </div>
                </div>
                <div class="maintintuc_down">
                    @foreach ($Download as $item)
                        
                   
                    <p><a href="{{$item->link}}">{{$item->name}}</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="{{$item->link}}">Tải về</a></p><br>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
    <hr>
</section>
<!-- khách hàng của chúng tôi -->


@endsection

@section('script')
@parent
<script>
    $(".clickmenuhead")[0].classList.remove("active");
    $(".clickmenuhead")[3].classList.add("active");
</script>

@endsection
