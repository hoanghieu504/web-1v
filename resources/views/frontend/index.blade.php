@extends('frontend.master.master')
@section('title','Trang chủ')
@section('trangchu','active')

@section('content')
@include("frontend.slideshow")
           
            
</header>
<div class="maintopindex">
    <!-- giới thiệu -->
    <section class="container maintop_stgt">
        <div class="overlay">
            <h2>SAG</h2>
            <p>SAG tự hào là đơn vị cung cấp dịch vụ tư vấn chuyên nghiệp trong các lĩnh vực: tư vấn thành lập doanh nghiệp,
                tư
                vấn tài
                chính, dịch vụ kế toán – thuế,
                đào tạo thực hành kế toán và cung cấp phần mềm kế toán cho các doanh nghiệp.</p>
        </div>
    </section>
    <!-- đặc điểm nổi bật -->
    <section class="container">
        <h2 class="maintop_h2">ĐẶC ĐIỂM NỔI BẬT CỦA 1V</h2>
        <div>
            <div class="row">
                @foreach($highlightt as $hl)
                <div class="card col-12 col-sm-12 col-md-4 maintop_stcards_clone" style="width: 18rem;">
                    <img src="image/{{$hl->image}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title" style="margin:auto">{{$hl->title}}</h5>
                        <p class="card-text">{!!$hl->content!!}</p>
                    </div>
                    </div>
                @endforeach

                
    
            </div>
        </div>
    </section>
    <!-- nghiệp vụ -->
    <section class="container">
        <h2 class="maintop_h2">MỌI NGHIỆP VỤ BẠN CẦN CHÚNG TÔI ĐỀU CÓ</h2>
        <div class="maintop_why_video">
            <iframe width="100%" height="500px" src="https://www.youtube.com/embed/udYzc66YVug?controls=0" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="row maintop_st_nv">
            <div class="col-12 col-sm-4">
                <ul>
                    <li><a class="maintop_st_nv_li_active" href="##"><i class="fas fa-caret-right"></i>Quỹ</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Ngân hàng điện tử</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Mua hàng</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Bán hàng</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Hợp đồng</a></li>
                </ul>
            </div>
    
            <div class="col-12 col-sm-4">
                <ul>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Quản lý hóa đơn</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Hóa đơn điện tử</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Thuế</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Kho</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Công cụ dụng cụ</a></li>
                </ul>
            </div>
    
            <div class="col-12 col-sm-4">
                <ul>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Tài sản cố định</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Tiền lương</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Giá thành</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Tổng hợp</a></li>
                    <li><a href="##"><i class="fas fa-caret-right"></i>Ngân sách</a></li>
                </ul>
            </div>
    
        </div>
    </section>
    <!-- TẠI SAO BẠN NÊN CHỌN PHẦN MỀM KẾ TOÁN CỦA CHÚNG TÔI -->
    <section class="container">
        <h2 class="maintop_h2">TẠI SAO BẠN NÊN CHỌN PHẦN MỀM KẾ TOÁN CỦA CHÚNG TÔI</h2>
        <div>
            <div class="row">
                <div class="col-sm">
                   
                     @foreach($highlightt1 as $hl1)
                     <div class="card mb-3 maintop_st_card_clone">
                        <div class="row no-gutters">
                            <div class="col-md-2 maintop_st_cardimg">
                                 <img src="image/{{$hl1->image}}" class="card-img-top" alt="...">
                            </div>
                            <div class="col-md-10">
                                <div class="card-body">
                                    <h5 class="card-title">{{$hl1->title}}</h5>
                                    <p class="card-text">{!!$hl1->content!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
    
                    
    
                 
                </div>
                <div class="col-sm">
                   
                        @foreach($highlightt2 as $hl2)
                        <div class="card mb-3 maintop_st_card_clone">
                        <div class="row no-gutters">
                            <div class="col-md-2 maintop_st_cardimg">
                              <img src="{{url('frontend/image/'.$hl2->image)}}" class="card-img-top" alt="...">
                            </div>
                            <div class="col-md-10">
                                <div class="card-body">
                                    <h5 class="card-title">{{$hl2->title}}</h5>
                                    <p class="card-text">{!!$hl2->content!!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                        @endforeach
    
                    
    
                  
                </div>
            </div>
            {{--  <div class="maintop_why_video">
                <iframe width="50%" height="315" src="https://www.youtube.com/embed/udYzc66YVug?controls=0" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
            </div>  --}}
        </div>
    </section>
    
</div>  

@endsection
