<!DOCTYPE html>
<html lang="en">

    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.0/css/all.css"
            integrity="sha384-REHJTs1r2ErKBuJB0fCK99gCYsVjwxHrSU0N7I1zl9vZbggVJXRMsv/sLlOAGb4M"
            crossorigin="anonymous" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
	    <link rel="stylesheet"
		href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
        <link rel="stylesheet" href="{{asset('frontend/style/stylefooter.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/style/styleheader.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/style/styleslideshow.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/style/stylemaintop.css')}}">
        <link rel="stylesheet" href="{{asset('frontend/style/style.css')}}">
    </head>
    <body>
        <header class="container-fluid">
            @include("frontend.master.menuheader")
        </header>
        <main id="mainindex" class="container-fluid">
            <div class="row">
                <div class="hdsd_left col-4 col-sm-4 col-md-4">
                    <nav class="hdsd_menuleft">
                        <ul class="hdsd_menuleft_group">
                            <li><a  class="hdsd_menuleft_link hdsd_menuleft_link_title hdsd_menuleft_active"><i
                                        class="fas fa-minus">Bắt đầu với 1v</i> </a>
                                        
                                <ul class="menu_hdsd_c2 menu_hdsd_active">
                                    @foreach($categories as $value) 
                                    @if($value->parent==0)    
                                        <li> <a class="hdsd_menuleft_link_c2" ><i class="fas fa-plus">{{$value->category_name}}</i></a>
                                            
                                                <ul class="menu_hdsd_last">
                                                
                                                    @foreach($categories as $value2) 
                                                    @if($value2->parent==$value->id)
                                                        <li>
                                                            <a href="hdsd/{{$value2->id}}" class="" >
                                                                <i class="fas fa-caret-right">{{$value2->category_name}}</i>
                                                            </a>
                                                        </li>
                                                        @endif
                                                        @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                    @endforeach
                                 
                                 </ul>
                            </li>
                        </ul>
                    </nav>
                    
                </div>
                <div class="hdsd_right col-8 col-sm-8 col-md-8">
                    <div class="hdsdn showhdsd"
                    <section class="container htdm_ctn">
                       
                            
                        
                            
                        
                        <h1 class="text-center h1_center">{{ $category->category_name}}</h1>
                        {{--  <h1 class="text-center h1_center">{{ $contents1->links}}</h1>  --}}
                        
                            
                        
                         
                        @foreach($contents as $ct)
                        
                        
                        <p>{!! $ct->content!!}</p>
                        <div class="maintop_why_video">
                            <iframe width="560" height="315" src="{{$ct->links}}" frameborder="0"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        
                        @endforeach
                        
                    </section>
                     </div>
                    <div class="hdsdn">@include("frontend.hethongdanhmuc")</div>
                    @include("frontend.hdsdfooter")
                </div>
            </div>
            
        </main>
        <footer class="container-fluid">
            @include("frontend.master.footer")
            
        </footer>

        <script src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
    <script src="script/script.js"></script>
    </body>

</html>
