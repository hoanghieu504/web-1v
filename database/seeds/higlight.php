<?php

use Illuminate\Database\Seeder;

class higlight extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('highlight')->delete();
        DB::table('highlight')->insert([
            ['id'=>1,'image'=>'unnamed.png','title'=>'ĐƠN GIẢN & DỄ DÙNG','content'=>'Nhân viên bán hàng chỉ mất 15 phút làm quen để bắt đầu bán hàng với phần mềm 1V
            . Giao diện đơn giản, thân thiện, thông
            minh giúp bạn triển khai quản lý bán hàng thật dễ dàng và nhanh chóng.','status'=>'1'],
            ['id'=>2,'image'=>'team.png','title'=>'PHÙ HỢP CHO TỪNG NGÀNH HÀNG','content'=>'PHÙ HỢP CHO TỪNG NGÀNH HÀNG</h5>
            <p class="card-text">Cùng với các chuyên gia bán hàng dày kinh nghiệm, chúng tôi nghiên cứu thiết kế
                phần mềm phù hợp đến hơn 15 ngành hàng
                dành cho cả bán buôn lẫn bán lẻ.','status'=>'1'],
            ['id'=>3,'image'=>'images.png','title'=>'BẢO HÀNH DÀI HẠN','content'=>'Miễn phí cài đặt, phí triển khai, nâng cấp và hỗ trợ. Rẻ hơn một ly trà đá, chỉ
            từ 5.000 đồng/ngày, bạn đã có thể áp
            dụng công nghệ vào quản lý cửa hàng.','status'=>'1'],
            ['id'=>4,'image'=>'25626313-thank-you-sign-icon-customer-service-symbol-red-award-label-with-stars-and-ribbons-vector.jpg','title'=>'THƯƠNG HIỆU UY TÍN','content'=>'Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng – Tổng cục Thuê ngay trên phần mềm.','status'=>'2'],
            ['id'=>5,'image'=>'lets-go-rocket-icon.png','title'=>'TIÊN PHONG CÔNG NGHỆ','content'=>'Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
            Tổng cục Thuê ngay trên phần mềm.','status'=>'2'],
            ['id'=>6,'image'=>'unnamedt.png','title'=>'TỐI ƯU HÓA HIỆU QUẢ LÀM VIỆC','content'=>'Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng – Tổng cục Thuê ngay trên phần mềm.','status'=>'2'],
            ['id'=>7,'image'=>'Tư-vấn-Icon.png','title'=>'TƯ VẤN TẬN TÌNH','content'=>'Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng – Tổng cục Thuê ngay trên phần mềm.','status'=>'3'],
            ['id'=>8,'image'=>'icon-thuyet-trinh-400x400.png','title'=>'ĐÀO TẠO HỌC ONLINE MIỄN PHÍ','content'=>'Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
            Tổng cục Thuê ngay trên phần mềm.','status'=>'3'],
            ['id'=>9,'image'=>'pngtree-refresh-icon-for-your-project-png-image_4845338.jpg','title'=>'CẬP NHẬT KỊP THỜI','content'=>'Tích hợp Hóa đơn điện tử, Ngân hàng điện tử và kết nối thẳng –
            Tổng cục Thuê ngay trên phần mềm.','status'=>'3'],

        ]);
    }
}
