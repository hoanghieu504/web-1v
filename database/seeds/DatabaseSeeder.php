<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        {
            $this->call(users::class);
            $this->call(category::class);
            $this->call(Content::class);
            $this->call(Product::class);
            $this->call(Product_package::class);
            $this->call(tintuc::class);
            $this->call(Slide::class);
            $this->call(logo::class);
            $this->call(higlight::class);
           
        }
    }
}
