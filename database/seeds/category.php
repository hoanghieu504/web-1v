<?php

use Illuminate\Database\Seeder;

class category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_category')->delete();
        DB::table('tbl_category')->insert([
            ['id'=>1,'category_name'=>'Hệ thống danh mục','slug'=>'he-thong-danh-muc','parent'=>0],
            ['id'=>2,'category_name'=>'Lý do tăng tài sản','slug'=>'ly-do-tang-tai-san','parent'=>1],
            ['id'=>3,'category_name'=>'Mục đích sử dụng','slug'=>'muc-dich-su-dung','parent'=>1],
            ['id'=>4,'category_name'=>'Số dư đầu','slug'=>'su-du-dau','parent'=>0],
            ['id'=>5,'category_name'=>'Hệ thống chứng từ','slug'=>'he-thong-chung-tu','parent'=>0],
            ['id'=>6,'category_name'=>'Tiện ích','slug'=>'tien-ich','parent'=>0],
            ['id'=>7,'category_name'=>'Lý do tăng tài sản','slug'=>'ly-do-tang-tai-san','parent'=>6],
            ['id'=>8,'category_name'=>'Mục đích sử dụng','slug'=>'muc-dich-su-dung','parent'=>6],
            ['id'=>9,'category_name'=>'Lý do tăng tài sản','slug'=>'ly-do-tang-tai-san','parent'=>4],
            ['id'=>10,'category_name'=>'Lý do tăng tài sản','slug'=>'ly-do-tang-tai-san','parent'=>5],
            ['id'=>11,'category_name'=>'Mục đích sử dụng','slug'=>'muc-dich-su-dung','parent'=>5]
           
        ]);
    }
}
