<?php

use Illuminate\Database\Seeder;

class tintuc extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post')->delete();
        DB::table('post')->insert([
            ['id'=>1,'title'=>'Giám đốc Công ty Luật Trường Sơn nhận xét về SAG','image'=>'khachhang1.jpg','contents'=>'Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp chủ doanh nghiệp vốn không có chuyên môn sâu về kế toán – tài chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính của doanh nghiệp để đưa ra những quyết sách phù hợp trong ngắn hạn và dài hạn','content'=>'Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp chủ doanh nghiệp vốn không có chuyên môn sâu về kế toán – tài chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính của doanh nghiệp để đưa ra những quyết sách phù hợp trong ngắn hạn và dài hạn.'],
            ['id'=>2,'title'=>'Giám đốc Công ty Luật Trường Sơn nhận xét về SAG','image'=>'khachhang1.jpg','contents'=>'Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp chủ doanh nghiệp vốn không có chuyên môn sâu về kế toán – tài chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính của doanh nghiệp để đưa ra những quyết sách phù hợp trong ngắn hạn và dài hạn','content'=>'Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp chủ doanh nghiệp vốn không có chuyên môn sâu về kế toán – tài chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính của doanh nghiệp để đưa ra những quyết sách phù hợp trong ngắn hạn và dài hạn.'],

            ['id'=>3,'title'=>'Giám đốc Công ty Luật Trường Sơn nhận xét về SAG','image'=>'khachhang1.jpg','contents'=>'Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp chủ doanh nghiệp vốn không có chuyên môn sâu về kế toán – tài chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính của doanh nghiệp để đưa ra những quyết sách phù hợp trong ngắn hạn và dài hạn','content'=>'Dễ sử dụng, chính xác và linh hoạt, phần mềm 1V đã giúp chủ doanh nghiệp vốn không có chuyên môn sâu về kế toán – tài chính như tôi có thể nắm bắt nhanh chóng và dễ dàng về tình hình kinh doanh, tài chính của doanh nghiệp để đưa ra những quyết sách phù hợp trong ngắn hạn và dài hạn.']

        ]);
    }
}
