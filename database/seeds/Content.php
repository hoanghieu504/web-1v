<?php

use Illuminate\Database\Seeder;

class Content extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_content')->delete();
        DB::table('tbl_content')->insert([
            ['id'=>3,'content'=>'demo1','links'=>'https://www.youtube.com/embed/cOewsRyqHA0','category_id'=>'1'],
            ['id'=>4,'content'=>'demo14444','links'=>'https://www.youtube.com/embed/cOewsRyqHA0','category_id'=>'4'],
        ]);

    }
}
