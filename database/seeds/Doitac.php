<?php

use Illuminate\Database\Seeder;

class Doitac extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('doitac')->delete();
        DB::table('doitac')->insert([
            ['id'=>1,'image'=>'6511cc69.png','links_img'=>'1V'],
            ['id'=>2,'image'=>'doitac1-1-240x135.jpg','links_img'=>'1V'],
            ['id'=>3,'image'=>'doitac3-1.jpg','links_img'=>'1V'],
        ]);
    }
}
