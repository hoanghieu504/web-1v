<?php

use Illuminate\Database\Seeder;

class Slide extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_slide')->delete();
        DB::table('tbl_slide')->insert([
            ['id'=>1,'image'=>'PYxMcsIiJ.JPG','links_img'=>'1V'],
            ['id'=>2,'image'=>'QNtifZpWP.JPG','links_img'=>'1V'],
            ['id'=>3,'image'=>'slideshow2.JPG','links_img'=>'1V'],
        ]);
    }
}
