<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProductPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_product_package', function (Blueprint $table) {
            $table->Increments('id');
            
            $table->string('name');
            $table->string('image');
            $table->decimal('price', 18, 0)->default(0); 
            $table->text('description');
            $table->text('content');
            $table->integer('id_product')->unsigned();
            $table->foreign('id_product')->references('id')->on('tbl_product_package');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_product_package');
    }
}
