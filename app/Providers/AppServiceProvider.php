<?php

namespace App\Providers;
 use App\models\Product;
 use App\models\Slide;
 use App\Post;
 use App\models\Product_package;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.master.menuheader',function($view){
            $sanpham=Product::all();
            $view->with('sanpham',$sanpham);
        });
        view()->composer('frontend.slideshow',function($view){
            $slide=Slide::all();
            $view->with('slide',$slide);
        });
        view()->composer('frontend.master.footer',function($view){
            $post=Post::all();
            $view->with('post',$post);
        });
        view()->composer('frontend.master.footer',function($view){
            $package=Product_package::all();
            $view->with('package',$package);
        });
        Schema::defaultStringLength(191);
    }
}
