<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Highlight extends Model
{
    protected $table = "highlight";
    public $timestamps=false;
}
