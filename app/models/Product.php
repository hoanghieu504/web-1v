<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "tbl_product";
    public $timestamps=false;

    public function Product_package()
    {
    	return $this->hasMany('App\models\Product_package','id_product','id');
    }
}
