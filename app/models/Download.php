<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
   protected $table='download_values';
  
   public $timestamps=false;
}
