<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "tbl_category";
    public $timestamps=false;
    public function content()
    {
    	return $this->belongsTo('App\models\Content','category_id','id');
    }
}
