<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    
    protected $table = "tbl_content";
    public $timestamps=false;
    public function category()
    {
        return $this->belongsTo('App\models\Category', 'category_id', 'id');
    }
}
