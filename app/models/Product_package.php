<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Product_package extends Model
{
    protected $table = "tbl_product_package";
    public $timestamps=false;

    public function Product()
    {
    	return $this->belongsTo('App\models\Product','id_product','id');
    }
}
