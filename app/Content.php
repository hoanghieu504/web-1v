<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = "tbl_content";

    public function Category()
    {
    	return $this->belongsTo('App\Category','category_id','id');
    }
}
