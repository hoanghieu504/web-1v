<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class List_post extends Model
{
    protected $table = "list_post";

    public function Post()
    {
    	return $this->hasMany('App\Post','id_dm','id');
    }
}
