<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use List_post;

class List_postController extends Controller
{
     public function getList()
   {
   	return view('admin.list_post.list');
   }
   public function getAdd()
   {
   	return view('admin.list_post.add');
   }
    public function getEdit()
   {
   	return view('admin.list_post.edit');
   }
}
