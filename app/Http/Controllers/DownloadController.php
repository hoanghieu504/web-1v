<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Download;

class DownloadController extends Controller
{
   function getList(){
    $data['download']=Download::all();
       return view('admin.download.list',$data);
   }

   function getAdd(){
     
    return view('admin.download.add');
    }
    function postAdd(request $r){
        //dd($r->all());
      $download = new Download;
      $download->name=$r->name;
      $download->link=$r->link;
      $download->mota=$r->content;
      $download->save();
      return redirect('admin/download/add')->with('thongbao','Thêm thành công');
     }
 

    function getEdit($id){
        $data['download']=Download::find($id);
        return view('admin.download.edit',$data);
    }
    function postEdit(request $r , $id){
    $download =  Download::find($id);
      $download->name=$r->name;
      $download->link=$r->link;
      $download->mota=$r->content;
      $download->save();
      return redirect('admin/download/add')->with('thongbao','Sửa thành công');
    }
    function Del($id){
        $download=Download::find($id);
        Download::destroy($id);
      
        return redirect()->back()->with('thongbao','Đã xoá thành công!');
    }
}
