<?php

namespace App\Http\Controllers\frontend;
use App\models\Slide;
use App\models\Category;
use App\models\Content;
use App\models\Doitac;
use App\models\Product;
use App\models\Download;
use App\models\Highlight;
use App\models\Product_package;
use App\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
  
  
    
    function getIndex(){
        $data['highlightt'] = Highlight::where('status',1)->get();
        $data['highlightt1'] = Highlight::where('status',2)->get();
        $data['highlightt2'] = Highlight::where('status',3)->get();

        $data['doitac']= Doitac::all();
        return view('frontend.index',$data);
        
    }
  
    function getTintuc(){
      
        $data['doitac']= Doitac::all();
        $data['post']= Post::paginate(10);
        $data['Download']= Download::all();
        $data['Product']= Product_package::paginate(5);
        
        return view('frontend.tintuc',$data);
        
    }
    function getShowtintuc($id){
      
       $data['doitac']= Doitac::all();
        $data['post']= Post::find($id);
        $data['postlist']= Post::paginate(3);
        return view('frontend.showtintucclone',$data);
        
    }
    function getGioithieu(){
      
       $data['doitac']= Doitac::all();
        return view('frontend.gioithieu',$data);
        
    }
    function getLienhe(){
       
        $data['doitac']= Doitac::all();
        return view('frontend.lienhe',$data);
        
    }
    // function getSanpham($id){
    //    // $data['slide']= Slide::all();
    //     $data['Product']= Product::all();
    //     return view('frontend.sanpham',$data);
        
    // }
    
    // function getHdsd(){
      
    //     $data['categories']=Category::all();
    //     $data['content']=Content::all();
    //     return view('frontend.hdsd',$data);
        
    // }
    function getHdsd_cate($id){
       
        $data['categories']=Category::all();
        $data['content']=Content::all();
        $data['contents']=Content::where('category_id',$id)->get();
        $data['category']=Category::find($id);
      return view('frontend.hdsd_cate',$data);
        
    }
    function getThanhtoan(){
       
        return view('frontend.mainthanhtoan');
        
    }
    
 }
