<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Slide;
use App\models\Product;
use App\models\Download;

use App\models\Product_package;
class SpController extends Controller
{
    function getSp($id){
        $data['Download']= Download::all();
        $data['Product']= Product::all();
        $data['Products']= Product::find($id);
        $data['package']=Product_package::where('id_product',$id)->get();
        //$data['prd_package']= Product_package::all();
        return view('frontend.sanpham',$data);
    }
}
