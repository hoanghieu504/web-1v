<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemberController extends Controller
{
      public function getList()
   {
   	return view('admin.member.list');
   }
   public function getAdd()
   {
   	return view('admin.member.add');
   }
    public function getEdit()
   {
   	return view('admin.member.edit');
   }
}
