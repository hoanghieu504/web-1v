<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Str;
class PostController extends Controller
{
	  public function getList()
   {
      $posts=Post::all();
   	return view('admin.post.list',['posts'=>$posts]);
   }
   public function getAdd()
   {
   	return view('admin.post.add');
   }
   public function postAdd(Request $Request)
   {
       $this->validate($Request,
      [
        'title'=>'required|min:5|max:200',
        'content'=>'required',
        'content1'=>'required'
      ],[
        'title.required'=>'Bạn chưa nhập tiêu đề',
        'content.required'=>'Bạn chưa nhập tiêu đề',
        'title.min'=>'tiêu đề quá ngắn',
        'title.max'=>'tiêu đề không được quá 200 ký tự',
        'content1.required'=>'Bạn chưa nhập tiêu đề'
      ]);
  
      $post= new Post;
      $post->title=$Request->title;
      $post->contents=$Request->content;
      $post->content=$Request->content1;
      if($Request->hasFile('image'))
           {
            $file = $Request->file('image');
            $format= $file->getClientOriginalExtension();
            if($format != 'jpg' && $format != 'png' && $format != 'jpeg' && $format !='JPEG' && $format != 'JPG' && $format != 'PNG')
            {
               return redirect('admin/post/add')->with('thongbao','bạn chỉ được chọn file jpg,png,jpeg,mp4');
            }
            $name = $file->getClientOriginalName();
            $image = str::random(5)."_".$name;
           
            while (file_exists("image/".$image)) {
              $image = str::random(5)."_".$name;
            }
            //unlink("public/upload/introduce/".$introduce->image);
            $file->move("image",$image);
            $post->image=$image;
             
           }
      $post->save();
    
    return redirect ('admin/post/list')->with('thongbao','Thêm thành công');
   }
    public function getEdit($id)
   {
      $post=Post::find($id);
   	return view('admin.post.edit',['post'=>$post]);
   }
    public function postEdit(Request $Request,$id)
   {
      //  $this->validate($Request,
      //  [
      //   'title'=>'required|min:5|max:200',
      //   'content'=>'required',
      //   'content1'=>'required'
      // ],[
      //   'title.required'=>'Bạn chưa nhập tiêu đề',
      //   'content.required'=>'Bạn chưa nhập tiêu đề',
      //   'title.min'=>'tiêu đề quá ngắn',
      //   'title.max'=>'tiêu đề không được quá 200 ký tự',
      //   'content1.required'=>'Bạn chưa nhập tiêu đề'
      // ]);
  
      $post= Post::find($id);
      $post->title=$Request->title;
      $post->contents=$Request->content;
      $post->content=$Request->content1;
      if($Request->hasFile('image'))
           {
            $file = $Request->file('image');
            $format= $file->getClientOriginalExtension();
            if($format != 'jpg' && $format != 'png' && $format != 'jpeg' && $format !='JPEG' && $format != 'JPG' && $format != 'PNG')
            {
               return redirect('admin/post/add')->with('thongbao','bạn chỉ được chọn file jpg,png,jpeg,mp4');
            }
            $name = $file->getClientOriginalName();
            $image = str::random(5)."_".$name;
           
            while (file_exists("image/".$image)) {
              $image = str::random(5)."_".$name;
            }
            if(isset($post->image))
            {
            unlink("image/".$post->image);
         }
            $file->move("image",$image);
            $post->image=$image;
             
           }
      $post->save();
    
    return redirect ('admin/post/list')->with('thongbao','Edit thành công');
   }
   public function getDelete($id)
   {
      $post=post::find($id);
       if(file_exists($post->img))
            {
            unlink("image/".$post->image);
         }
        
      $post->delete();
 return redirect ('admin/post/list')->with('thongbao','Xóa thành công');
   }
}
