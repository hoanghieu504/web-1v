<?php

namespace App\Http\Controllers;
use App\models\Slide;
use Illuminate\Http\Request;

class BaseController extends Controller
{
  public function __construct()
  {
    $slide	=	Slide::all();

    View::share('slide', $slide);
  }
}
