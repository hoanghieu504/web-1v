<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\information;
use Illuminate\Support\Str;
class InformationController extends Controller
{
    //
    public function index()
    {
    	$information=information::all();
    	return view('admin.information.index',['information'=>$information]);
    }
     public function create()
    {
   
    	return view('admin.information.create');
    }
     public function store(Request $Request)
    {
    	  $information = information::all();
    if($information->count()>=1)
    {
      return redirect ('admin/information/index')->with('thongbao','Qúa số bản ghi');
    }
      $information= new information;
      $information->name=$Request->name;
      $information->registerd_address=$Request->registerd_address;
       $information->active_address=$Request->active_address;
        $information->tax=$Request->tax;
         $information->phone=$Request->phone;
          $information->hotline=$Request->hotline;
           $information->email=$Request->email;
            $information->website=$Request->website;
             $information->map=$Request->map;
              $information->introduce=$Request->introduce;
             
 if($Request->hasFile('logo'))
           {
            $file = $Request->file('logo');
            $format= $file->getClientOriginalExtension();
            if($format != 'jpg' && $format != 'png' && $format != 'jpeg' && $format !='JPEG' && $format != 'JPG' && $format != 'PNG')
            {
               return redirect('admin/information/add')->with('thongbao','bạn chỉ được chọn file jpg,png,jpeg,mp4');
            }
            $name = $file->getClientOriginalName();
            $logo = str::random(5)."_".$name;
           
            while (file_exists("public/upload/logo/".$logo)) {
              $logo = str::random(5)."_".$name;
            }
            //unlink("public/upload/introduce/".$introduce->image);
            $file->move("public/upload/logo",$logo);
            $information->logo=$logo;
             
           }
     
      $information->save();
    
    return redirect ('admin/information/index')->with('thongbao','Thêm thành công');
   }
   
    	
    	public function getEdit($id)
    	{
    		$information=information::find($id);
    	return view('admin.information.show',['information'=>$information]);

    	}
    	  public function postEdit(Request $Request,$id)
    {
    	
  
      $information = information::find($id);
      $information->name=$Request->name;
      $information->registerd_address=$Request->registerd_address;
       $information->active_address=$Request->active_address;
        $information->tax=$Request->tax;
         $information->phone=$Request->phone;
          $information->hotline=$Request->hotline;
           $information->email=$Request->email;
            $information->website=$Request->website;
             $information->map=$Request->map;
              $information->introduce=$Request->introduce;
             
      if($Request->hasFile('logo'))
           {
            $file = $Request->file('logo');
            $format= $file->getClientOriginalExtension();
            if($format != 'jpg' && $format != 'png' && $format != 'jpeg' && $format !='JPEG' && $format != 'JPG' && $format != 'PNG')
            {
               return redirect('admin/information/create')->with('thongbao','bạn chỉ được chọn file jpg,png,jpeg,mp4');
            }
            $name = $file->getClientOriginalName();
            $logo = str::random(5)."_".$name;
           
            while (file_exists("public/upload/logo/".$logo)) {
              $logo = str::random(5)."_".$name;
            }
             if(isset($information->logo))
            {
            unlink("public/upload/logo/".$information->logo);
         }
            //unlink("public/upload/introduce/".$introduce->image);
            $file->move("public/upload/logo",$logo);
            $information->logo=$logo;
             
           }
     
      $information->save();
    
    return redirect ('admin/information/index')->with('thongbao','Thêm thành công');
   }
   

    
}
