<?php

namespace App\Http\Controllers;
use App\models\Category;
use Illuminate\Http\Request;
use App\models\users;
use App\User;
use DB;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Str;
class CategoryController extends Controller
{
   public function getList()
   {
      $data['categories']=Category::all();
     // $data['cate']=Category::find($idCate);
   	return view('admin.category.list',$data);
   }
   public function getAdd()
   {
      $data['categories']=Category::all();
      //dd($data);
   	return view('admin.category.add',$data);
   }

   public function postAdd(CategoryRequest $r)
   {
      
      $cate = new Category;
      $cate->category_name=$r->txtCateName;
      $cate->slug=Str::slug($r->txtCateName, '-');
      $cate->parent=$r->parent;
      
      $cate->save();
      
      return redirect()->back()->with('thongbao','Đã thêm thành công');
   }


    public function getEdit($idCate)
   {
      $data['categories']=Category::all();
      $data['cate']=Category::find($idCate);
   	return view('admin.category.edit',$data);
   }

   public function postEdit($idCate,CategoryRequest $r)
   {
      $cate=Category::find($idCate);
      $cate->category_name=$r->txtCateName;
      $cate->slug=Str::slug($r->txtCateName, '-');
      $cate->parent=$r->parent;
      $cate->save();
      return redirect()->back()->with('thongbao','đã sửa thành công');
   }

 // xoa danh muc
   function postDel($idCate){
      $cate=Category::find($idCate);
      Category::where('parent',$cate->id)->update(["parent"=>"$cate->parent"]);
      $cate->delete();
      return redirect()->back()->with('thongbao','Đã xoá thành công!');
     
  }
}
