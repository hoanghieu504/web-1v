<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Doitac;

class DoitacController extends Controller
{
    public function getList()
    {
      $doitac = Doitac::all();
      return view('admin.doitac.list',['doitac'=> $doitac]);
    }
    public function getAdd()
    {
      return view('admin.doitac.add');
    }
    public function postAdd(Request $request)
    {
      $this->validate($request,
         [
                'links_img' => 'required'
         ],
         [
                'links_img.required' => 'Bạn chưa nhập links ảnh'
         ]);
      $doitac = new Doitac;
      $doitac->links_img = $request->links_img;

      if($request->hasFile('image'))
        {
         $file = $request->file('image');
         $duoi = $file->getClientOriginalExtension();
         if($duoi != 'jpg' && $duoi !='png' && $duoi !='jpeg'&& $duoi != 'JPG')
         {
            return redirect('admin/doitac/list')->with('loi','Bạn chỉ được chọn file có đuôi jpg, png , jpeg');
         }
         $name = $file->getClientOriginalName();
         $image = $name;
         while(file_exists("frontend/image".$image))
         {
            $image = $name;
            }
            $file->move("uploads/doitac",$image);
            $doitac->image = $image;
        }
        $doitac->save();
        return redirect('admin/doitac/add')->with('thongbao','Bạn đã thêm thành công');
    }
    public function getEdit($id)
    {
       $doitac = Doitac::find($id);
      return view('admin.doitac.edit',['doitac'=>$doitac]);
    }
    public function postEdit(Request $request,$id)
    {
       $this->validate($request,
         [
                'links_img' => 'required'
         ],
         [
                'links_img.required' => 'Bạn chưa nhập links ảnh'
         ]);
      $doitac = Doitac::find($id);
      $doitac->links_img = $request->links_img;

      if($request->hasFile('image'))
        {
         $file = $request->file('image');
         $duoi = $file->getClientOriginalExtension();
         if($duoi != 'jpg' && $duoi !='png' && $duoi !='jpeg' )
         {
            return redirect('admin/doitac/list')->with('loi','Bạn chỉ được chọn file có đuôi jpg, png , jpeg');
         }
         $name = $file->getClientOriginalName();
         $image = $name;
         while(file_exists("frontend/image".$image))
         {
            $image = $name;
            }
            $file->move("uploads/doitac",$image);
            $doitac->image = $image;
        }
        $doitac->save();
        return redirect('admin/doitac/edit/'.$id)->with('thongbao','Bạn đã sửa thành công');
    }
    public function getDelete($id)
    {
      $doitac = Doitac::find($id);
      $doitac->delete();
      return redirect('admin/doitac/list')->with('thongbao','Bạn đã xóa thành công');
    }
}
