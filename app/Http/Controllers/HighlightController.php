<?php

namespace App\Http\Controllers;
use App\models\Highlight;
use Illuminate\Http\Request;

class HighlightController extends Controller
{
    public function getList()
    {
         $highlight = Highlight::all();
        return view('admin.highlight.list',['highlight'=>$highlight]);
    }
    public function getAdd()
    {
        return view('admin.highlight.add');
    }
    public function postAdd(Request $request){
       //kiem tra bien cos nhap dung yeeu cau hay khoong
        $this->validate($request,
       [
          //highlight
          'image' =>'required',
          'title' => 'required',
          'content'=>'required',
          'status' => 'required|numeric'
          //customer
         
          
       ],
       [
          //highlight
          'image.required'=>'BBạn chưa nhập image',
           'content.required'=>'Bạn chưa nhập content',
          'status.required'=>'Bạn chưa nhập tatus',
          'status.numeric' =>'status phải là số "1 - Đặc điểm nổi bật" or "2 - Tại sao cột 1" or "3 - Tại sao cột 2"',
          'title.required'=>'Bạn chưa nhập title'
          //customer  
         
       ]);
       $highlight = new Highlight;
       $highlight->title =$request->title;
       $highlight->content =$request->content;
       $highlight->status = $request->status;
         if ($request->hasFile('image')) {
          $file = $request->file('image');
          $duoi = $file->getClientOriginalExtension();
          if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
             return redirect('admin/highlight/add')->with('loi','không đúng định dạng');
          }
          $name = $file->getClientOriginalName();
          $image = $name;
          while(file_exists("frontend/image".$image)){
             $image = $name;
          }
          $file->move("frontend/image",$image);
          $highlight->image=$image;
       }
 
       $highlight->save();
 
       return redirect('admin/highlight/add')->with('thongbao','Thêm thành công');
    }
     public function getEdit($id)
    {
        $highlight = Highlight::find($id);
        return view('admin.highlight.edit',['highlight'=>$highlight]);
    }
    public function postEdit(Request $request,$id) //gửi giữ liệu lên phải khai báo Request / và bên route có khai báo thêm $id
    {
         $this->validate($request,
       [
          //highlight
          'title' => 'required',
          'content'=>'required',
          'status' => 'required|numeric'
         
          
       ],
       [
          //highlight
          'status.required'=>'Bạn chưa nhập tatus',
          'content.required'=>'Bạn chưa nhập content',
          'status.numeric' =>'status phải là số "1 - Đặc điểm nổi bật" or "2 - Tại sao cột 1" or "3 - Tại sao cột 2"',
          'title.required'=>'Bạn chưa nhập title'
         
       ]);
       $highlight = Highlight::find($id);
       // $highlight = new Highlight;
       $highlight->title =$request->title;
       $highlight->content =$request->content;
       $highlight->status = $request->status;
         if ($request->hasFile('image')) {
           $file = $request->file('image');
           $duoi = $file->getClientOriginalExtension();
          if ($duoi !='jpg'&&$duoi !='png'&&$duoi !='jpeg' &&$duoi !='gif') {
             return redirect('admin/highlight/edit/'.$id)->with('loi','không đúng định dạng');
          }
          $name = $file->getClientOriginalName();
          $image = $name;
          while(file_exists("frontend/image".$image)){
             $image = $name;
          }
          $file->move("frontend/image",$image);
          $highlight->image=$image;
       }
 
       $highlight->save();
       return redirect('admin/highlight/edit/'.$id)->with('thongbao','Bạn sửa thành công');
    }
  
    public function getDel($id)
    {
       $highlight = Highlight::find($id);
       $highlight->delete();
       return redirect('admin/highlight/list')->with('thongbao','Bạn xóa thành công');
    }
 }
 

