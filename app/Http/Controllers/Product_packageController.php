<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\models\Product;
use App\models\Product_package;
use App\Http\Requests\Product_packageRequest;

class Product_packageController extends Controller
{
    public function getList()
    {
    	$product_package = Product_package::all();
    	return view('admin.product_package.list',['product_package'=>$product_package]);
    }
    public function getAdd()
    {
    	$product = Product::all();
    	return view('admin.product_package.add',['product'=>$product]);
    }
    public function postAdd(Product_packageRequest $request)
    {
    	
        $product_package = new Product_package;

        $product_package->name = $request->name;
        $product_package->content = $request->content;
        $product_package->price = $request->price;
        $product_package->description = $request->description;
        $product_package->content  = $request->content;
        $product_package->id_product  = $request->sanpham;

        if($request->hasFile('image'))
        {
        	$file = $request->file('image');
        	$duoi = $file->getClientOriginalExtension();
        	if($duoi != 'jpg' && $duoi !='png' && $duoi !='jpeg' )
        	{
        		return redirect('admin/product_package/add')->with('loi','Bạn chỉ được chọn file có đuôi jpg, png , jpeg');
        	}
        	$name = $file->getClientOriginalName();
        	$image = $name;
        	while(file_exists("image".$image))
        	{
        		$image = $name;
         	}
         	$file->move("image",$image);
         	$product_package->image = $image;
        }

        $product_package->save();
        return redirect('admin/product_package/add')->with('thongbao','Bạn đã thêm thành công');
    }
    public function getEdit($id)
    {
          $data['Product'] = Product::all();
          $data['product_package'] = Product_package::find($id);
          return view('admin.product_package.edit',$data);
    }
    public function postEdit(Product_packageRequest $request,$id)
    {
    	$product_package = Product_package::find($id);
    	

           $product_package->name = $request->name;
           $product_package->content = $request->content;
           $product_package->price = $request->price;
           $product_package->description = $request->description;
           $product_package->content  = $request->content;
           $product_package->id_product  = $request->sanpham;

        if($request->hasFile('image'))
        {
        	$file = $request->file('image');
        	$duoi = $file->getClientOriginalExtension();
        	if($duoi != 'jpg' && $duoi !='png' && $duoi !='jpeg' )
        	{
        		return redirect('admin/product_package/edit')->with('loi','Bạn chỉ được chọn file có đuôi jpg, png , jpeg');
        	}
        	$name = $file->getClientOriginalName();
        	$image = $name;
        	while(file_exists("image".$image))
        	{
        		$image = $name;
         	}
         	$file->move("image",$image);
         	$product_package->image = $image;
        }

        $product_package->save();
        return redirect('admin/product_package/edit/'.$id)->with('thongbao','Bạn đã sửa thành công');
    }
    public function getDelete($id)
    {
    	$product_package = Product_package::find($id);
    	$product_package->delete();
    	return redirect('admin/product_package/list')->with('thongbao','Bạn đã xóa thành công');
    }
}
