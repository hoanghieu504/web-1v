<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Product;
use App\Http\Requests\ProductRequest;

class ProductController extends Controller
{
   public function getList()
      {
      $product = Product::all();
         return view('admin.product.list',['product'=>$product]);
      }
   public function getAdd()
      {
         return view('admin.product.add');
      }
   public function postAdd(ProductRequest $request)
   {
     // dd($request->all());
   
      $product = new Product;
      $product->name = $request->name;
      $product->content = $request->content;
      $product->tinhnang = $request->content1;
      $product->save();
        return redirect('admin/product/add')->with('thongbao','Bạn đã thêm thành công');
   }

     public function getEdit($id)
       {
        $product = Product::find($id);
       	return view('admin.product.edit',['product'=>$product]);
       }
     public function postEdit(ProductRequest $request,$id)
     {
      //   $this->validate($request,
      //   [
      //           'name' => 'required',
      //           'description' => 'required',
      //           'featue' => 'required'

      //   ],
      //       [
      //           'name.required' =>'Bạn chưa nhập tên danh mục',
      //           'description.required' =>'Bạn chưa nhập mô tả',
      //           'featue.required' =>'Bạn chưa nhập đặc tính'

      //   ]);
       $product = Product::find($id);
       $product->name = $request->name;
       $product->content = $request->content;
       $product->tinhnang = $request->content1;
       $product->save();
        
        return redirect('admin/product/edit/'.$id)->with('thongbao','Bạn đã sửa thành công');
     }
     public function getDelete($id)
     {
        $product = Product::find($id);
        $product->delete();
        return redirect('admin/product/list')->with('thongbao','Bạn đã xóa thành công');
     }

}
