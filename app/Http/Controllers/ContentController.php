<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use App\Content;
use App\models\Content;
use App\models\Category;
use App\Http\Requests\ContentRequest;
class ContentController extends Controller
{
      public function getList()
   {
      $data['content']= Content::all();
      $data['cate']= Category::all();
   	return view('admin.content.list',$data);
   }
   public function getAdd()
   {
      $data['categories']=Category::all();
   	return view('admin.content.add',$data);
   }
   /// Thêm nội dung
   public function postAdd(ContentRequest $r )
   {
      //dd($r->all());
      $content= new Content;
     
      $content->content = $r->description;
      $content->links = $r->link;
      $content->category_id = $r->category ;
      $content->save();
       return redirect()->back()->with('thongbao','Đã thêm thành công');

      
   }
   public function postEdit($id ,ContentRequest $r )
   {
      //dd($r->all());
      $content= Content::find($id);
     
      $content->content = $r->description;
      $content->links = $r->link;
      $content->category_id = $r->category ;
      $content->save();
       return redirect('admin/content/list')->with('thongbao','Đã sửa thành công');

      
   }
    public function getEdit($id)
   {
      $data['categories']=Category::all();
      $data['cate']=Category::find($id);
      $data['content']=Content::find($id);
   	return view('admin.content.edit',$data);
   }
   ////- xoa danh muc
   public function getDel($id)
   {
      //$content=Content::find($id);
     
      Content::destroy($id);
      
      return redirect()->back()->with('thongbao','Đã xoá thành công!');
   }
   
}
