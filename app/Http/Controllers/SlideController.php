<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\Slide;

class SlideController extends Controller
{
    public function getList()
    {
      $slide = Slide::all();
      return view('admin.slide.list',['slide'=> $slide]);
    }
    public function getAdd()
    {
      return view('admin.slide.add');
    }
    public function postAdd(Request $request)
    {
      $this->validate($request,
         [
                'links_img' => 'required'
         ],
         [
                'links_img.required' => 'Bạn chưa nhập links ảnh'
         ]);
      $slide = new Slide;
      $slide->links_img = $request->links_img;

      if($request->hasFile('image'))
        {
         $file = $request->file('image');
         $duoi = $file->getClientOriginalExtension();
         if($duoi != 'jpg' && $duoi !='png' && $duoi !='jpeg'&& $duoi != 'JPG')
         {
            return redirect('admin/slide/add')->with('loi','Bạn chỉ được chọn file có đuôi jpg, png , jpeg');
         }
         $name = $file->getClientOriginalName();
         $image = $name;
         while(file_exists("frontend/image".$image))
         {
            $image = $name;
            }
            $file->move("uploads/slide",$image);
            $slide->image = $image;
        }
        $slide->save();
        return redirect('admin/slide/add')->with('thongbao','Bạn đã thêm thành công');
    }
    public function getEdit($id)
    {
       $slide = Slide::find($id);
      return view('admin.slide.edit',['slide'=>$slide]);
    }
    public function postEdit(Request $request,$id)
    {
       $this->validate($request,
         [
                'links_img' => 'required'
         ],
         [
                'links_img.required' => 'Bạn chưa nhập links ảnh'
         ]);
      $slide = Slide::find($id);
      $slide->links_img = $request->links_img;

      if($request->hasFile('image'))
        {
         $file = $request->file('image');
         $duoi = $file->getClientOriginalExtension();
         if($duoi != 'jpg' && $duoi !='png' && $duoi !='jpeg' )
         {
            return redirect('admin/slide/list')->with('loi','Bạn chỉ được chọn file có đuôi jpg, png , jpeg');
         }
         $name = $file->getClientOriginalName();
         $image = $name;
         while(file_exists("frontend/image".$image))
         {
            $image = $name;
            }
            $file->move("uploads/slide",$image);
            $slide->image = $image;
        }
        $slide->save();
        return redirect('admin/slide/edit/'.$id)->with('thongbao','Bạn đã sửa thành công');
    }
    public function getDelete($id)
    {
      $slide = Slide::find($id);
      $slide->delete();
      return redirect('admin/slide/list')->with('thongbao','Bạn đã xóa thành công');
    }
}
