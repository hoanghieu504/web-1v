<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function getList()
   {
    $Customers = Customer::all();
   	return view('admin.customer.list',['Customers'=>$Customers]);
   }
   public function getAdd()
   {
   	return view('admin.customer.add');
   }
     public function postAdd(Request $Request)
   {
      $this->validate($Request,
      [
        'title'=>'required|min:20|max:200',
        'content'=>'required'
      ],[
        'title.required'=>'Bạn chưa nhập tiêu đề',
        'content.required'=>'Bạn chưa nhập tiêu đề',
        'title.min'=>'tiêu đề quá ngắn',
        'title.max'=>'tiêu đề không được quá 200 ký tự'
      ]);
    $Customers = Customer::all();
    if($Customers->count()>2)
    {
      return redirect ('admin/customer/list')->with('thongbao','Qúa số bản ghi');
    }
      $Customer= new Customer;
      $Customer->title=$Request->title;
      $Customer->content=$Request->content;

      $Customer->save();
    
    return redirect ('admin/customer/list')->with('thongbao','Thêm thành công');
   }
    public function getEdit($id)
   {
    $Customer=Customer::find($id);
   	return view('admin.customer.edit',['Customer'=>$Customer]);
   }
    public function postEdit(Request $Request,$id)
   {
      $this->validate($Request,
      [
        'title'=>'required|min:20|max:200',
        'content'=>'required'
      ],[
        'title.required'=>'Bạn chưa nhập tiêu đề',
        'content.required'=>'Bạn chưa nhập tiêu đề',
        'title.min'=>'tiêu đề quá ngắn',
        'title.max'=>'tiêu đề không được quá 200 ký tự'
      ]);
  
      $Customer=Customer::find($id);
      $Customer->title=$Request->title;
      $Customer->content=$Request->content;

      $Customer->save();
    
    return redirect ('admin/customer/list')->with('thongbao','Thêm thành công');
   }
   public function getDelete($id)
   {
 $Customer=Customer::find($id);
 $Customer->delete();
  return redirect ('admin/customer/list')->with('thongbao','Xóa thành công');
   }
}
