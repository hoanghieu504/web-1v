<?php

namespace App\Http\Controllers;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

use App\models\users;
use App\User;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('admin.login');
    }
    function postLogin(LoginRequest $r){
      
        if(Auth::attempt(['email' => $r->email, 'password' => $r->password])){
           
            return redirect('admin/product/add');
        }
        else{
         return redirect()->back()->withErrors(['email'=>'Email hoac mat khau khong chinh xac'])->withInput();
        }


    }

    function getLogout(){
        Auth::logout();
        return redirect('login');
    }
}
