<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
     public function getList()
   {
   	return view('admin.user.list');
   }
   public function getAdd()
   {
   	return view('admin.user.add');
   }
    public function getEdit()
   {
   	return view('admin.user.edit');
   }
}
