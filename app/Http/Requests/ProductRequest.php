<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'content1' => 'required'
        ];
    }
    public function messages() 
    {
        return [
            'name.required' =>'Bạn chưa nhập tên danh mục',
            'content.required' =>'Bạn chưa nhập Giới Thiệu',
            'content1.required' =>'Bạn chưa nhập Tính năng'
        ];
    }
}
