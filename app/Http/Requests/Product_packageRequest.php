<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Product_packageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'price' => 'required|numeric',
           
            'description' => 'required',
            
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên gói sản phẩm',
            'content.required' => 'Bạn chưa nhập nội dung',
            'price.required' => 'Bạn chưa nhập giá sản phẩm',
            'price.numeric' => 'Bạn chưa nhập giá sản phẩm',
            'description.required' => 'Bạn chưa thêm mô tả',
            
        ];
    }
}
