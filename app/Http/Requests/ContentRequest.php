<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'=>'required',
            'link'=>'required',
            'category'=>'required|unique:tbl_content,category_id',
        ];
    }
    public function messages()
    {
        return [
            'description.required'=>'Nội dung không được để trống',
            'link.required'=>'Link không được để trống',
            'category.unique'=>'Trùng danh mục',
        ];
    }
}
