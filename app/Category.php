<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "tbl_category";

    public function content()
    {
    	return $this->hasMany('App\Content','category_id','id');
    }
}

