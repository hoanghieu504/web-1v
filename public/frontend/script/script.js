$(".hdsd_menuleft_link_c2").click(function () {
    $link = [];
    $ul = [];
    $count = 0;
    // lấy class list menu c2
    for ($i = 0; $i < $(".hdsd_menuleft_link_c2").length; $i++) {
        $link[$i] = $(".hdsd_menuleft_link_c2")[$i];
    }
    // lấy class group last
    for ($i = 0; $i < $(".menu_hdsd_last").length; $i++) {
        $ul[$i] = $(".menu_hdsd_last")[$i];
    }
    // xóa trạng thái kích hoạt của 1 group
    for ($i = 0; $i < $(".hdsd_menuleft_link_c2").length; $i++) {
        if ($link[$i] == this) {
            $count = $i;
        }
    }
    if (this.classList == "hdsd_menuleft_link_c2") {
        // trạng thái active khi click vào thẻ li
        this.classList.add("hdsd_menuleft_active");
        // show menu cấp dưới ở đây là cấp cuối
        $ul[$count].classList.add("menu_hdsd_active");
        $(".hdsdn")[2].classList.add("showhdsd");
        $(".hdsdn")[1].classList.remove("showhdsd");
        $(".hdsdn")[0].classList.remove("showhdsd");
    } else {
        // trạng thái active khi click vào thẻ li
        this.classList.remove("hdsd_menuleft_active");
        // show menu cấp dưới ở đây là cấp cuối
        $ul[$count].classList.remove("menu_hdsd_active");
    }
});
$(".hdsd_menuleft_link_last").click(function () {
    $link = [];
    for ($i = 0; $i < $(".hdsd_menuleft_link_last").length; $i++) {
        $link[$i] = $(".hdsd_menuleft_link_last")[$i];
    }
    this.classList.add("hdsd_menuleft_active");
    $(".hdsdn")[2].classList.remove("showhdsd");
    $(".hdsdn")[1].classList.remove("showhdsd");
    $(".hdsdn")[0].classList.add("showhdsd");
    for ($i = 0; $i < $(".hdsd_menuleft_link_last").length; $i++) {
        if ($link[$i] != this) {
            $link[$i].classList.remove("hdsd_menuleft_active");
        }
    }
});
$count_active_ctsp = 0;
$list_bgsp = [];
for ($i = 0; $i < $(".list_bgsp").length; $i++){
    $list_bgsp[$i] = $(".list_bgsp")[$i];
}
$ctsp_clone_main = [];
for ($i = 0; $i < $(".ctsp_clone_main").length; $i++) {
    $ctsp_clone_main[$i] = $(".ctsp_clone_main")[$i];
}
$(".click_showctsp").click(function () {
    for ($i = 0; $i < $(".click_showctsp").length; $i++) {
        if (this == $(".click_showctsp")[$i]) {
            $ctsp_clone_main[$i].classList.add("active");
            for ($j = 0; $j < $(".click_showctsp").length; $j++) {
                if ($j != $i) {
                    $ctsp_clone_main[$j].classList.remove("active");
                }
            }
            $(".list_bgsp")[$i].classList.add("active");
            for ($j = 0; $j < $(".click_showctsp").length; $j++) {
                if ($j != $i) {
                    $list_bgsp[$j].classList.remove("active");
                }
            }
        }
    }
})

$(document).ready(function () {

    $('.owl-carousel').owlCarousel({
        autoplay: true,
        autoplayHoverPause: true,
        items: 4,
        nav: true,
        loop: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 4,
                nav: true
            },
            500: {
                items: 4,
                nav: true
            },
            768: {
                items: 4,
                nav: true
            },
            1000: {
                items: 4,
                nav: true
            }
        }
    });



});
